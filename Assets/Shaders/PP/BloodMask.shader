﻿Shader "Custom/PP/BloodMask"
{
    HLSLINCLUDE

        #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"

        TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
        float4 _Color;
        float _Opacity;
        TEXTURE2D_SAMPLER2D(_Mask, sampler_Mask);

        float4 Frag(VaryingsDefault i) : SV_Target
        {
            float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord);
            half vfactor = SAMPLE_TEXTURE2D(_Mask, sampler_Mask, i.texcoord).a;
			
            #if !UNITY_COLORSPACE_GAMMA
            {
                vfactor = SRGBToLinear(vfactor);
            }
            #endif
			
            half3 new_color = color.rgb * lerp(_Color, (1.0).xxx, vfactor);
            color.rgb = lerp(color.rgb, new_color, _Opacity);
            color.a = lerp(1.0, color.a, vfactor);

            return color;
        }

    ENDHLSL

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            HLSLPROGRAM

                #pragma vertex VertDefault
                #pragma fragment Frag

            ENDHLSL
        }
    }
}