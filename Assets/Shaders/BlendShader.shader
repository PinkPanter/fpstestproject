﻿Shader "Custom/Unlit/BlendShader"
{
    Properties
    {
		_Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Texture", 2D) = "white" {}
        _MainTex ("Texture", 2D) = "white" {}
        _BlendableTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float4 _Color;

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _BlendableTex;
            float4 _BlendableTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 mainTex = tex2D(_MainTex, i.uv);
				fixed4 bland = tex2D(_BlendableTex, i.uv);
                fixed4 col = fixed4(((mainTex.rgb * (1 - bland.a)) + (bland.rgb * bland.a)), 1) * _Color;
                return col;
            }
            ENDCG
        }
    }
}
