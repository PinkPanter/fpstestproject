﻿using System;
using System.Collections;
using FPSTestProject.Characters;
using FPSTestProject.Helpers.Runtime.ObjectPool;
using FPSTestProject.Managers;
using FPSTestProject.Weapons.Pool.BloodPools;
using FPSTestProject.Weapons.Pool.BulletHolePools;
using FPSTestProject.Weapons.Pool.BulletPools;
using FPSTestProject.Weapons.Pool.GrenadePools;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FPSTestProject
{
    public class Arena : MonoBehaviour
    {
        #region Serializable

        [SerializeField]
        private GameObject[] warriors;

        [SerializeField]
        private Transform[] points;

        [SerializeField]
        [Range(0, 50)]
        private float spawnTime = 5;

        [SerializeField]
        [Range(0, 50)]
        private int spawnCountIncreaseEach = 5;

        [SerializeField]
        [Range(0, 50)]
        private int spawnCount = 3;

        #endregion

        private float lastSpawnTime;

        private int wave;

        private int currentPointIndex;

        private void Awake()
        {
            PoolInitializer.GetPool<Bullet>();
            PoolInitializer.GetPool<BulletHole>();
            PoolInitializer.GetPool<Blood>();
            PoolInitializer.GetPool<Grenade>();
        }

        private void Start()
        {
            CharacterHolder.LocalPlayer.OnDead+=LocalPlayerOnOnDead;
        }

        private void LocalPlayerOnOnDead()
        {
            StartCoroutine(WaitAndLoad());
        }

        private IEnumerator WaitAndLoad()
        {
            yield return new WaitForSeconds(2);
            LoadingManager.LoadScene(1);
        }

        // Update is called once per frame
        private void Update()
        {
            if (Time.time - lastSpawnTime >= spawnTime)
            {
                lastSpawnTime = Time.time;
                Spawn();
                wave++;

                if (wave % spawnCountIncreaseEach == 0)
                {
                    spawnCount++;
                }
            }
        }

        private void Spawn()
        {
            for (int i = 0; i < spawnCount; i++)
            {
                var currentPoint = points[currentPointIndex];

                currentPointIndex++;
                if (currentPointIndex >= points.Length)
                    currentPointIndex = 0;

                Instantiate(warriors[Random.Range(0, warriors.Length - 1)], currentPoint.position, Quaternion.identity);
            }
        }
    }
}
