﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FPSTestProject.Managers
{
    public class LoadingManager : MonoBehaviour
    {
        #region Serializable

        [SerializeField]
        private Camera renderCamera;
        [SerializeField]
        private RawImage textureTarget;

        #endregion

        private static LoadingManager instance;

        private void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
                return;
            }

            RenderTexture texture;
            if (SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGB1555))
                texture = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB1555);
            else
                texture = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB32);

            texture.Create();

            renderCamera.targetTexture = texture;
            textureTarget.texture = texture;

            instance = this;
            DontDestroyOnLoad(gameObject);

            LoadScene(1);
        }

        public static void LoadScene(int sceneIndex)
        {
#if UNITY_EDITOR
            if (instance == null)
            {
                SceneManager.LoadScene(sceneIndex);
                return;
            }
#endif

            instance.gameObject.SetActive(true);
            instance.StartCoroutine(LoadSceneCor(sceneIndex));
        }

        public static void LoadScene(string sceneName)
        {
#if UNITY_EDITOR
            if (instance == null)
            {
                SceneManager.LoadScene(sceneName);
                return;
            }
#endif

            instance.gameObject.SetActive(true);
            instance.StartCoroutine(LoadSceneCor(sceneName));
        }

        private static IEnumerator LoadSceneCor(int sceneIndex)
        {
            var loading = SceneManager.LoadSceneAsync(sceneIndex);

            while (!loading.isDone)
            {
                yield return null;
            }

            yield return new WaitForSecondsRealtime(1);

            instance.gameObject.SetActive(false);
        }

        private static IEnumerator LoadSceneCor(string sceneName)
        {
            var loading = SceneManager.LoadSceneAsync(sceneName);

            while (!loading.isDone)
            {
                yield return null;
            }

            yield return new WaitForSecondsRealtime(1);

            instance.gameObject.SetActive(false);
        }
    }
}
