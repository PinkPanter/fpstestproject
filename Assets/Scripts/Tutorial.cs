﻿using System;
using System.Collections;
using FPSTestProject.Characters;
using FPSTestProject.Helpers.Runtime.ObjectPool;
using FPSTestProject.Managers;
using FPSTestProject.Weapons;
using FPSTestProject.Weapons.Pool.BloodPools;
using FPSTestProject.Weapons.Pool.BulletHolePools;
using FPSTestProject.Weapons.Pool.BulletPools;
using FPSTestProject.Weapons.Pool.GrenadePools;
using UnityEngine;

namespace FPSTestProject
{
    public class Tutorial : MonoBehaviour
    {
        #region Serializable

        [SerializeField]
        private Spawnable[] spawnables;

        #endregion

        private Tuple<Spawnable, float, GameObject>[] chache;

        private void Awake()
        {
            PoolInitializer.GetPool<Bullet>();
            PoolInitializer.GetPool<BulletHole>();
            PoolInitializer.GetPool<Blood>();
            PoolInitializer.GetPool<Grenade>();

            chache = new Tuple<Spawnable, float, GameObject>[spawnables.Length];
            for (int i = 0; i < chache.Length; i++)
            {
                chache[i] = new Tuple<Spawnable, float, GameObject>(spawnables[i], -200, null);

                if (spawnables[i].firstTimePlace != null)
                {
                    var spawned = Instantiate(chache[i].Item1.prefab);
                    spawned.transform.position = spawnables[i].firstTimePlace.position;

                    var weapon = spawned.GetComponent<Weapon>();
                    if (weapon != null)
                    {
                        weapon.TransformToItem();
                        SetLayerToObjectAndChilds(weapon.transform, LayerMask.NameToLayer("Default"));
                    }
                }
            }
        }

        private void Start()
        {
            CharacterHolder.LocalPlayer.OnDead += LocalPlayerOnOnDead;
        }

        private void LocalPlayerOnOnDead()
        {
            StartCoroutine(WaitAndLoad());
        }

        private IEnumerator WaitAndLoad()
        {
            yield return new WaitForSeconds(2);
            LoadingManager.LoadScene(1);
        }

        private void Update()
        {
            for (var i = 0; i < chache.Length; i++)
            {
                var tuple = chache[i];
                if (Time.time - tuple.Item2 >= tuple.Item1.delay && tuple.Item3 == null)
                {

                    var spawned = Instantiate(tuple.Item1.prefab);
                    spawned.transform.position = tuple.Item1.place.position;

                    var weapon = spawned.GetComponent<Weapon>();
                    if (weapon != null)
                    {
                        weapon.TransformToItem();
                        SetLayerToObjectAndChilds(weapon.transform, LayerMask.NameToLayer("Default"));
                    }

                    chache[i] = new Tuple<Spawnable, float, GameObject>(tuple.Item1, Time.time, spawned);
                }
            }
        }

        private static void SetLayerToObjectAndChilds(Transform activeTransform, int layer)
        {
            activeTransform.gameObject.layer = layer;
            foreach (Transform childs in activeTransform)
            {
                SetLayerToObjectAndChilds(childs, layer);
            }
        }

        [Serializable]
        private struct Spawnable
        {
            public GameObject prefab;

            public float delay;

            public Transform place;

            public Transform firstTimePlace;
        }
    }
}
