﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace FPSTestProject.Editor.Tools
{
    public class TagToAll
    {
        public const string TagToChild = "Tools/Tags/Set current tag to childs";

        [MenuItem(TagToChild)]
        public static void SetTagToChildren()
        {
            var currentTag = Selection.activeGameObject.tag;

            List<Object> objectsToUndo = new List<Object>();
            SetTagToObjectAndChilds(Selection.activeTransform, currentTag, objectsToUndo);
            Undo.RecordObjects(objectsToUndo.ToArray(), "Set tags");
        }

        private static void SetTagToObjectAndChilds(Transform activeTransform, string currentTag, List<Object> objectsToUndo)
        {
            objectsToUndo.Add(activeTransform.gameObject);
            activeTransform.gameObject.tag = currentTag;
            foreach (Transform childs in activeTransform)
            {
                SetTagToObjectAndChilds(childs, currentTag, objectsToUndo);
            }
        }
    }
}
