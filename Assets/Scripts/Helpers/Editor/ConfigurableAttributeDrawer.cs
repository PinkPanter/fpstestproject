﻿using System.Globalization;
using FPSTestProject.Helpers.Runtime.Configs;
using UnityEditor;
using UnityEngine;

namespace FPSTestProject.Helpers.Configs.Editor
{
    [CustomPropertyDrawer(typeof(ConfigurableAttribute))]
    public class ConfigurableAttributeDrawer : PropertyDrawer
    {
        private bool initialized;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 2f;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var realName = $"{property.serializedObject.targetObject.GetType().Name}.{property.name}";

            if (!initialized)
            {
                if (property.propertyType == SerializedPropertyType.String)
                {
                    var value = ConfigController.GetString(realName);
                    if (value != null)
                        property.stringValue = value;
                }
                else if (property.propertyType == SerializedPropertyType.Float)
                {
                    var value = ConfigController.GetFloat(realName);
                    if (value != null)
                        property.floatValue = value.Value;
                }
                else if (property.propertyType == SerializedPropertyType.Integer)
                {
                    var value = ConfigController.GetInt(realName);
                    if (value != null)
                        property.intValue = value.Value;
                }

                initialized = true;
            }

            EditorGUI.PropertyField(new Rect(position.position, new Vector2(position.size.x, position.size.y / 2f)),
                property, label);

            EditorGUI.LabelField(new Rect(position.position + new Vector2(0, position.size.y / 2f), 
                new Vector2(position.size.x /2f, position.size.y / 2f)), realName);

            if (GUI.Button(new Rect(position.position + new Vector2(position.size.x / 2f, position.size.y / 2f),
                new Vector2(position.size.x / 2f, position.size.y / 2f)), "Save"))
            {
                if (property.propertyType == SerializedPropertyType.String)
                {
                    ConfigController.Save(realName, property.stringValue);
                }
                else if (property.propertyType == SerializedPropertyType.Float)
                {
                    ConfigController.Save(realName, property.floatValue.ToString("F3", CultureInfo.InvariantCulture));
                }
                else if (property.propertyType == SerializedPropertyType.Integer)
                {
                    ConfigController.Save(realName, property.intValue.ToString());
                }
            }
        }
    }
}
