﻿using System.Globalization;
using UnityEngine;

namespace FPSTestProject.Helpers.Runtime.Configs
{
    public class ConfigurableMonoBehaviour : MonoBehaviour
    {
        protected virtual void Awake()
        {
            ConfigController.ValueUpdated += ConfigControllerOnValueUpdated;
            ConfigController.InitializeObject(this);
        }

        private void ConfigControllerOnValueUpdated(string key, string value)
        {
            var indexOfDot = key.IndexOf('.');
            var type = key.Remove(indexOfDot, key.Length - indexOfDot);
            if (type == GetType().Name)
            {
                var fieldName = key.Remove(0, indexOfDot + 1);
                foreach (var fieldInfo in ConfigController.GetConfigurableFields(GetType()))
                {
                    if (fieldInfo.Name == fieldName)
                    {
                        if (fieldInfo.FieldType == typeof(int))
                        {
                            if (int.TryParse(value, out var outInt))
                                fieldInfo.SetValue(this, outInt);
                        }
                        else if (fieldInfo.FieldType == typeof(float))
                        {
                            if (float.TryParse(value, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,
                                out var outFloat))
                                fieldInfo.SetValue(this, outFloat);
                        }
                        else if (fieldInfo.FieldType == typeof(string))
                        {
                            fieldInfo.SetValue(this, value);
                        }
                    }
                }
            }
        }

        protected virtual void OnDestroy()
        {
            ConfigController.ValueUpdated -= ConfigControllerOnValueUpdated;
        }
    }
}
