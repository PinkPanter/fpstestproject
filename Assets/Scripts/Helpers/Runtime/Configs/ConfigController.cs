﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace FPSTestProject.Helpers.Runtime.Configs
{
    public static class ConfigController
    {
        private const string configFileName = "Configs.ini";
        private const string configDefFileName = "DefConfigs.ini";

        public static event Action<string, string> ValueUpdated = delegate { };

        private static readonly Dictionary<Type, FieldInfo[]> configurableFields = new Dictionary<Type, FieldInfo[]>();

        private static Dictionary<string, string> parced;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void AutoInitialize()
        {
            //if(!Debug.isDebugBuild)
            //return;

            var typeInhFrom = typeof(ConfigurableMonoBehaviour);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => !p.IsAbstract && typeInhFrom.IsAssignableFrom(p));

            foreach (var type in types)
            {
                CollectFields(type);                
            }
        }

        static ConfigController()
        {
            if (!File.Exists($"{Application.persistentDataPath}/{configFileName}"))
                if (File.Exists($"{Application.streamingAssetsPath}/{configDefFileName}"))
                    File.Move($"{Application.streamingAssetsPath}/{configDefFileName}",
                        $"{Application.persistentDataPath}/{configFileName}");

            parced = ParceFile($"{Application.persistentDataPath}/{configFileName}");
        }

        public static void InitializeObject(ConfigurableMonoBehaviour configurableMonoBehaviour)
        {
            if (configurableFields.ContainsKey(configurableMonoBehaviour.GetType()))
            {
                InitializeField(configurableMonoBehaviour, configurableFields[configurableMonoBehaviour.GetType()]);
                return;
            }

            CollectFields(configurableMonoBehaviour.GetType());
            InitializeField(configurableMonoBehaviour, configurableFields[configurableMonoBehaviour.GetType()]);
        }

        public static FieldInfo[] GetConfigurableFields(Type type)
        {
            if (configurableFields.ContainsKey(type))
                return configurableFields[type];

            CollectFields(type);
            return configurableFields[type];
        }

        public static IReadOnlyDictionary<Type, FieldInfo[]> GetAllConfigurableFields()
        {
            return configurableFields;
        }

        public static int? GetInt(string name)
        {
            if (parced.ContainsKey(name))
            {
                if (int.TryParse(parced[name], out var outInt))
                    return outInt;
            }

            return null;
        }

        public static string GetString(string name)
        {
            if (parced.ContainsKey(name))
            {
                return parced[name];
            }

            return null;
        }

        public static float? GetFloat(string name)
        {
            if (parced.ContainsKey(name))
            {
                if (float.TryParse(parced[name], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var outInt))
                    return outInt;
            }

            return null;
        }

        public static void Save(string name, string data)
        {
            WriteToFile(name, data);

#if UNITY_EDITOR
            WriteToEditor(name, data);
#endif

            if (Application.isPlaying)
                ValueUpdated(name, data);
        }

        private static void WriteToFile(string name, string data)
        {
            if (parced.ContainsKey(name))
                parced[name] = data;
            else
                parced.Add(name, data);

            parced = parced.OrderBy(e => e.Key).ToDictionary(k => k.Key, v => v.Value);

            WriteToFile($"{Application.persistentDataPath}/{configFileName}", parced);
        }

        private static void WriteToFile(string path, Dictionary<string, string> parced)
        {
            if (!Directory.Exists(Path.GetDirectoryName(path)))
                Directory.CreateDirectory(Path.GetDirectoryName(path));

            StringBuilder stringBuilder = new StringBuilder(path.Length * 2);
            string currentType = string.Empty;

            foreach (var element in parced)
            {
                var indexOfPoint = element.Key.IndexOf('.');
                var elementType = element.Key.Remove(indexOfPoint, element.Key.Length - indexOfPoint);
                if (elementType != currentType)
                {
                    currentType = elementType;
                    stringBuilder.AppendLine($"#{elementType}");
                }

                stringBuilder.AppendLine($"{element.Key.Remove(0, indexOfPoint+1)}-{element.Value}");
            }

            File.WriteAllText(path, stringBuilder.ToString());
        }

        private static Dictionary<string, string> ParceFile(string path)
        {
            Dictionary<string, string> parced = new Dictionary<string, string>();

            if (!File.Exists(path))
                return parced;

            var sr = new StreamReader(path);
            string line;
            string currentType = string.Empty;

            while ((line = sr.ReadLine()) != null)
            {
                if (line[0] == '#')
                {
                    currentType = line.Remove(0, 1);
                }
                else if (currentType != string.Empty)
                {
                    var splitIndex = line.IndexOf('-');
                    var currentName = line.Remove(splitIndex, line.Length - splitIndex);
                    var currentValue = line.Remove(0, splitIndex + 1);
                    parced.Add(currentType+"."+ currentName, currentValue);
                }
            }

            sr.Close();

            return parced;
        }

        private static void CollectFields(Type type)
        {
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .Where(prop => prop.IsDefined(typeof(ConfigurableAttribute))).ToArray();

            configurableFields.Add(type, fields);
        }

        private static void InitializeField(ConfigurableMonoBehaviour configurableMonoBehaviour, FieldInfo[] configurableField)
        {
            foreach (FieldInfo fieldInfo in configurableField)
            {
                var key = $"{configurableMonoBehaviour.GetType().Name}.{fieldInfo.Name}";
                if (fieldInfo.FieldType == typeof(int))
                {
                    var value = GetInt(key);

                    if (value != null)
                        fieldInfo.SetValue(configurableMonoBehaviour, value.Value);
                }
                else if (fieldInfo.FieldType == typeof(float))
                {
                    var value = GetFloat(key);
                    if (value != null)
                        fieldInfo.SetValue(configurableMonoBehaviour, value.Value);
                }
                else if (fieldInfo.FieldType == typeof(string))
                {
                    var value = GetString(key);
                    if (value != null)
                        fieldInfo.SetValue(configurableMonoBehaviour, value);
                }
            }
        }


#if UNITY_EDITOR
        private static void WriteToEditor(string name, string data)
        {
            Dictionary<string, string> editorParced = ParceFile($"{Application.streamingAssetsPath}/{configDefFileName}");
            if (editorParced.ContainsKey(name))
                editorParced[name] = data;
            else
                editorParced.Add(name, data);

            editorParced = editorParced.OrderBy(e => e.Key).ToDictionary(k => k.Key, v => v.Value);

            WriteToFile($"{Application.streamingAssetsPath}/{configDefFileName}", editorParced);
        }
#endif
    }
}
