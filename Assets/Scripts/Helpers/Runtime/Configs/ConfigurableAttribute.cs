﻿using UnityEngine;

namespace FPSTestProject.Helpers.Runtime.Configs
{
    [System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
    public class ConfigurableAttribute : PropertyAttribute
    {
        public string displayName;
    }
}
