﻿using UnityEngine;

namespace FPSTestProject.Helpers.Runtime.SoundManager
{
    public enum SoundType
    {
        Footstep = 0,

        BulletFlash = 10,
        BulletTile = 20,

        GrenadeThrow = 30,
        GrenadeExplosion = 40,

        PistolEquip = 50,
        PistolReload = 60,
        PistolFire = 70,
        PistolDry = 80,

        ShotgunEquip = 90,
        ShotgunReload = 100,
        ShotgunFire = 110,
        ShotgunDry = 120,

        RifleEquip = 130,
        RifleReload = 140,
        RifleFire = 150,
        RifleDry = 160,

        Collect = 170,
        Heal = 180,

        UiClick = 1000,
    }
}
