﻿using System;
using FPSTestProject.Characters;
using FPSTestProject.Characters.Player;
using FPSTestProject.Helpers.Runtime.ObjectPool;
using FPSTestProject.Helpers.Runtime.SoundManager;
using FPSTestProject.Weapons.Pool.BulletPools;
using UnityEngine;

namespace FPSTestProject.Weapons
{
    public class Weapon : MonoBehaviour
    {
        #region Serializable

        [SerializeField]
        protected Sprite icon;

        [SerializeField]
        protected new BoxCollider collider;

        [SerializeField]
        protected SphereCollider triger;

        [SerializeField]
        protected Vector3 handShift;

        [SerializeField]
        protected Vector3 handRot;

        [SerializeField]
        protected RuntimeAnimatorController animatorController;

        [SerializeField]
        protected int animType;

        [SerializeField]
        protected Animator animator;

        [SerializeField]
        protected bool automatic;

        [SerializeField]
        [Range(1, 200)]
        protected int clipCount;
        
        [SerializeField]
        [Range(0f, 10f)]
        protected float fireTimeout = 1;

        [SerializeField]
        [Range(1f, 45f)]
        protected float recoil = 15;

        [SerializeField]
        protected Transform aimPoint;

        [SerializeField]
        protected Transform shootPoint;

        [SerializeField]
        [Range(1, 200)]
        protected float bulletSpeed = 200;

        [SerializeField]
        protected int bulletDamage = 20;

        [SerializeField]
        protected ParticleSystem[] mazles;

        [SerializeField]
        protected SoundType equipSound;

        [SerializeField]
        protected SoundType reloadSound;

        [SerializeField]
        protected SoundType fireSound;

        [SerializeField]
        protected SoundType dryFireSound;

        #endregion

        public event Action OnWeaponShoot = delegate { }; 

        public bool IsLock { get; set; }

        public Vector3 HandShift => handShift;

        public Vector3 HandRot => handRot;

        public RuntimeAnimatorController AnimatorController => animatorController;

        public int AnimType => animType;

        public bool Automatic => automatic;

        public float Recoil => recoil;

        public int ClipCount => clipCount;

        public int TotalBullets { get; set; }

        public int CurrentClipCount { get; private set; }

        public Vector3 AimPos => aimPoint.position;

        public Vector3 ShootDir => shootPoint.forward;

        public Vector3 ShootPos => shootPoint.position;

        public Sprite Icon => icon;

        public Vector3 FakeShootDir
        {
            set
            {
                useFakeDir = true;
                fakeDir = value;
            }
        }

        protected bool useFakeDir;

        protected Vector3 fakeDir;

        private float lastShootTime;

        private Vector3 originalPos;

        private Quaternion originalRot;

        private bool emptySoundWasPlayed = false;

        private void Awake()
        {
            originalPos = transform.localPosition;
            originalRot = transform.localRotation;
        }

        public virtual void Init(int bulletsCount, bool isNpc)
        {
            CurrentClipCount = 0;
            TotalBullets = bulletsCount;
            useFakeDir = false;
            ReloadEnd();
        }

        public virtual void Equip()
        {
            transform.localPosition = originalPos;
            transform.localRotation = originalRot;

            animator.SetTrigger("Equip");
            gameObject.SetActive(true);

            SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(equipSound), transform.position, 0.75f, transform);
        }

        public void Deequip()
        {
            gameObject.SetActive(false);
        }

        public bool Reload()
        {
            if (IsLock)
                return false;

            if (CurrentClipCount >= clipCount || TotalBullets <= 0)
                return false;

            SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(reloadSound), transform.position, 0.75f, transform);
            animator.SetTrigger("Reload");
            return true;
        }

        public bool Fire()
        {
            if (IsLock)
                return false;

            if (Time.time - lastShootTime < fireTimeout)
                return false;

            lastShootTime = Time.time;

            if (CurrentClipCount <= 0)
            {
                if (!emptySoundWasPlayed)
                    SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(dryFireSound), transform.position,
                        0.75f, transform, false, 0.1f);
                emptySoundWasPlayed = true;
                return false;
            }

            SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(fireSound), transform.position, 0.75f, transform, false, 0.1f);
            CurrentClipCount--;
            animator.SetTrigger("Shoot");
            ThrowBullet();
            return true;
        }
        
        public void ReloadEnd()
        {
            int needToLoad = clipCount - CurrentClipCount;
            if (TotalBullets >= needToLoad)
            {
                CurrentClipCount += needToLoad;
                TotalBullets -= needToLoad;
            }
            else
            {
                CurrentClipCount += needToLoad;
                TotalBullets = 0;
            }

            emptySoundWasPlayed = false;
        }

        public virtual void OnAim()
        { }

        public virtual void OnDeAim()
        { }

        public void TransformToItem()
        {
            gameObject.layer = LayerMask.NameToLayer("Items");
            transform.SetParent(null);

            var rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.mass = 3;

            collider.enabled = true;
            triger.enabled = true;
        }

        public void TransformToUsagable()
        {
            Destroy(gameObject.GetComponent<Rigidbody>());
            collider.enabled = false;
            triger.enabled = false;
        }

        public Vector3 InverseDirectionByShootPoint(Vector3 direction)
        {
            return shootPoint.InverseTransformDirection(direction);
        }

        protected virtual void ThrowBullet()
        {
            foreach (ParticleSystem mazle in mazles)
            {
                mazle.Play(false);
            }

            OnWeaponShoot();
            if (useFakeDir)
            {
                PoolInitializer.GetPool<Bullet>().GetObject(shootPoint.position, Quaternion.LookRotation(fakeDir))
                    .Shoot(bulletSpeed, bulletDamage);
            }
            else
                PoolInitializer.GetPool<Bullet>().GetObject(shootPoint.position, Quaternion.LookRotation(shootPoint.forward))
                    .Shoot(bulletSpeed, bulletDamage);
        }

        private void OnTriggerEnter(Collider other)
        {
            var character = other.GetComponentInParent<CharacterHolder>();
            if (character != null)
            {
                character.GetCharacterComponent<PlayerWeaponController>()?.AddWeapon(this);
            }
        }
    }
}
