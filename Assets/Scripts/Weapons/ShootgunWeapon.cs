﻿using FPSTestProject.Helpers.Runtime.ObjectPool;
using FPSTestProject.Weapons.Pool.BulletPools;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FPSTestProject.Weapons
{
    public class ShootgunWeapon : Weapon
    {
        #region Serializable

        [SerializeField] [Range(2, 20)] protected int bulletsCount = 5;

        [SerializeField] [Range(0, 1)] protected float accuracy = 0.1f;

        #endregion

        protected override void ThrowBullet()
        {
            base.ThrowBullet();

            if (useFakeDir)
            {
                for (int i = 0; i < bulletsCount - 1; i++)
                {
                    PoolInitializer.GetPool<Bullet>().GetObject(shootPoint.position,
                            Quaternion.LookRotation(
                                fakeDir + shootPoint.InverseTransformDirection(Random.insideUnitCircle * accuracy)))
                        .Shoot(bulletSpeed, bulletDamage);
                }
            }
            else
            {
                PoolInitializer.GetPool<Bullet>()
                    .GetObject(shootPoint.position,
                        Quaternion.LookRotation(shootPoint.forward +
                                                shootPoint.InverseTransformDirection(
                                                    Random.insideUnitCircle * accuracy)))
                    .Shoot(bulletSpeed, bulletDamage);
            }
        }
    }
}

