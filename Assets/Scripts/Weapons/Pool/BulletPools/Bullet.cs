﻿using FPSTestProject.Characters;
using FPSTestProject.Helpers.Runtime.ObjectPool;
using FPSTestProject.Helpers.Runtime.SoundManager;
using FPSTestProject.Weapons.Pool.BloodPools;
using FPSTestProject.Weapons.Pool.BulletHolePools;
using UnityEngine;

namespace FPSTestProject.Weapons.Pool.BulletPools
{
    public class Bullet : MonoBehaviour, IPoolable
    {
        #region Serializable

        [SerializeField]
        private new Rigidbody rigidbody;

        [SerializeField]
        private LayerMask bodies;

        [SerializeField]
        private LayerMask nonStatic;

        #endregion

        public bool IsActive => gameObject.activeSelf;

        private int currentDamage;

        #region IPoolable

        public void Activate(Vector3 position, Quaternion rotation)
        {
            var cache = transform;
            cache.position = position;
            cache.rotation = rotation;

            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            rigidbody.velocity = Vector3.zero;
            gameObject.SetActive(false);
        }

        #endregion

        public void Shoot(float speed, int damage)
        {
            currentDamage = damage;
            rigidbody.AddRelativeForce(0, 0, speed, ForceMode.Impulse);
        }

        private void OnCollisionEnter(Collision other)
        {
            Deactivate();
            var currentContact = other.contacts[0];
            if (bodies != (bodies | (1 << other.gameObject.layer)))
            {
                SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.BulletTile),
                    currentContact.point, 0.2f, null, false, 0.1f);

                var hole = PoolInitializer.GetPool<BulletHole>().GetObject(currentContact.point,
                    Quaternion.LookRotation(currentContact.normal));

                if (nonStatic == (nonStatic | (1 << other.gameObject.layer)))
                    hole.AttachTo(currentContact.otherCollider.transform);
            }
            else
            {
                SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.BulletFlash),
                    currentContact.point, 0.5f, null, false, 0.1f);

                PoolInitializer.GetPool<Blood>().GetObject(currentContact.point,
                    Quaternion.LookRotation(currentContact.normal));

                var character = other.gameObject.GetComponentInParent<CharacterHolder>();
                if (character != null)
                    character.Damage((currentContact.otherCollider.CompareTag("Head") ? 3 : 1) * currentDamage);
            }
        }
    }
}
