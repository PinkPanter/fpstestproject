﻿using FPSTestProject.Helpers.Runtime.ObjectPool;

namespace FPSTestProject.Weapons.Pool.BulletPools
{
    public class BulletPool : ObjectPool<Bullet>
    {
        protected override bool KeepBetweenLevels => false;

        protected override int InitialCount => 200;

        protected override string Prefab => "Prefabs/Bullets/Bullet";
    }
}
