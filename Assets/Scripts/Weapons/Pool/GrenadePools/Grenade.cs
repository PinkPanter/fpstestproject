﻿using System;
using FPSTestProject.Characters;
using FPSTestProject.Helpers.Runtime.Configs;
using FPSTestProject.Helpers.Runtime.ObjectPool;
using FPSTestProject.Helpers.Runtime.SoundManager;
using UnityEngine;

namespace FPSTestProject.Weapons.Pool.GrenadePools
{
    public class Grenade : ConfigurableMonoBehaviour, IPoolable
    {
        #region Serializable

        [SerializeField]
        private new Rigidbody rigidbody;

        [SerializeField]
        [Configurable]
        [Range(0, 500)]
        private int damage = 300;

        [SerializeField]
        [Configurable]
        [Range(0, 10)]
        private float radius = 5;

        [SerializeField]
        [Range(0, 30)]
        private float throwStrength = 20;

        [SerializeField]
        [Range(0, 30)]
        private float explosionDelay = 3;

        [SerializeField]
        private GameObject explosion;

        [SerializeField]
        private GameObject grenadeRenderer;

        [SerializeField]
        [Range(0, 30)]
        private float lifeTimeAfterExplosion = 6;

        [SerializeField]
        private LayerMask affected;

        #endregion

        public bool IsActive => gameObject.activeSelf;

        private bool wasExplode;
        private float startTime;
        private Collider[] results = new Collider[20];

        #region IPoolable

        public void Activate(Vector3 position, Quaternion rotation)
        {
            var cache = transform;
            cache.position = position;
            cache.rotation = rotation;

            wasExplode = false;
            grenadeRenderer.SetActive(true);
            gameObject.SetActive(true);
            rigidbody.isKinematic = false;
        }

        public void Deactivate()
        {
            explosion.SetActive(false);
            gameObject.SetActive(false);
        }

        #endregion

        public void Throw(Vector3 direction)
        {
            rigidbody.AddForce(throwStrength * 60f * direction, ForceMode.Force);
            startTime = Time.time;
        }

        private void Update()
        {
            if (!wasExplode && Time.time - startTime >= explosionDelay)
            {
                Explode();
                return;
            }

            if (wasExplode && Time.time - startTime >= lifeTimeAfterExplosion)
            {
                Deactivate();
            }
        }

        private void Explode()
        {
            SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.GrenadeExplosion),
                transform.position, 1, null, true);

            wasExplode = true;
            rigidbody.isKinematic = true;
            grenadeRenderer.SetActive(false);
            startTime = Time.time;
            explosion.SetActive(true);

            var size = Physics.OverlapSphereNonAlloc(transform.position, radius, results, affected);
            for (int i = 0; i < size; i++)
            {
                var norm = (results[i].transform.position - transform.position).normalized;
                var dist = (radius - (results[i].transform.position - transform.position).magnitude) / radius;
                if (results[i].CompareTag("Body"))
                {
                    var character = results[i].GetComponentInParent<CharacterHolder>();
                    if (character != null)
                    {
                        character.DamageAndPush((int) (damage * dist), dist * damage * 100 * norm);
                    }
                    continue;
                }

                if (results[i].attachedRigidbody != null)
                {
                    results[i].attachedRigidbody.AddForce(dist * damage * 100 * norm, ForceMode.Force);
                }
            }
        }

        #if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, radius);
        }
        #endif
    }
}
