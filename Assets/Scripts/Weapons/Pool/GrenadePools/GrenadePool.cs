﻿using FPSTestProject.Helpers.Runtime.ObjectPool;

namespace FPSTestProject.Weapons.Pool.GrenadePools
{
    public class GrenadePool : ObjectPool<Grenade>
    {
        protected override bool KeepBetweenLevels => false;

        protected override int InitialCount => 40;

        protected override string Prefab => "Prefabs/Bullets/Grenade";
    }
}
