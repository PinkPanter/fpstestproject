﻿using FPSTestProject.Helpers.Runtime.ObjectPool;

namespace FPSTestProject.Weapons.Pool.BloodPools
{
    public class BloodPool : ObjectPool<Blood>
    {
        protected override bool KeepBetweenLevels => false;

        protected override int InitialCount => 50;

        protected override string Prefab => "Prefabs/BulletHoles/Blood";
    }
}
