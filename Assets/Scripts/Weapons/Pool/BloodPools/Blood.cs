﻿using FPSTestProject.Helpers.Runtime.ObjectPool;
using UnityEngine;

namespace FPSTestProject.Weapons.Pool.BloodPools
{
    public class Blood : MonoBehaviour, IPoolable
    {
        #region Serializable

        [SerializeField]
        [Range(0, 5)]
        private float lifetime = 1;

        #endregion
        public bool IsActive => gameObject.activeSelf;

        private float startTime;

        public void Activate(Vector3 position, Quaternion rotation)
        {
            transform.position = position;
            transform.rotation = rotation;
            startTime = Time.time;

            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        private void Update()
        {
            if(Time.time - startTime >= lifetime)
                Deactivate();
        }
    }
}
