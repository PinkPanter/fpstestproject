﻿using FPSTestProject.Helpers.Runtime.ObjectPool;
using UnityEngine;

namespace FPSTestProject.Weapons.Pool.BulletHolePools
{
    public class BulletHolePool : ObjectPool<BulletHole>
    {
        protected override bool KeepBetweenLevels => false;

        protected override int InitialCount => 200;

        protected override string Prefab => "Prefabs/BulletHoles/Hole1";
    }
}
