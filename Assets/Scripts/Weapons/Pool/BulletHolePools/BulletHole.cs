﻿using FPSTestProject.Helpers.Runtime.ObjectPool;
using UnityEngine;

namespace FPSTestProject.Weapons.Pool.BulletHolePools
{
    public class BulletHole : MonoBehaviour, IPoolable
    {
        #region Serializable

        [SerializeField]
        private float lifeTime = 10;

        #endregion

        public bool IsActive => gameObject.activeSelf;

        private float activationTime;

        private Transform originalParent;

        public void Activate(Vector3 position, Quaternion rotation)
        {
            gameObject.SetActive(true);
            transform.position = position;
            transform.rotation = rotation;
            originalParent = null;

            activationTime = Time.realtimeSinceStartup;
        }

        public void Deactivate()
        {
            if (originalParent != null)
                transform.SetParent(originalParent);

            gameObject.SetActive(false);
        }

        private void Update()
        {
            if(Time.realtimeSinceStartup - activationTime >= lifeTime)
                Deactivate();
        }

        public void AttachTo(Transform attachTo)
        {
            originalParent = transform.parent;
            transform.SetParent(attachTo);
        }
    }
}
