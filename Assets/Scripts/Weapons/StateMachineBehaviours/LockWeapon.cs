﻿using FPSTestProject.Characters;
using FPSTestProject.Characters.Controllers;
using UnityEngine;

namespace FPSTestProject.Weapons.StateMachineBehaviours
{
    public class LockWeapon : StateMachineBehaviour
    {
        [SerializeField]
        private bool lockAtStart = true;
        [SerializeField]
        private bool unlockAtStart = true;

        private WeaponController weaponController;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if(!lockAtStart)
                return;

            if (weaponController == null)
                weaponController = animator.GetComponentInParent<CharacterHolder>().GetCharacterComponent<WeaponController>();

            weaponController.LockWeapon();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!unlockAtStart)
                return;

            if (weaponController == null)
                weaponController = animator.GetComponentInParent<CharacterHolder>().GetCharacterComponent<WeaponController>();

            weaponController.UnlockWeapon();
        }
    }
}
