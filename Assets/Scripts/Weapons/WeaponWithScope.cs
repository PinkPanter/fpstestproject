﻿using UnityEngine;

namespace FPSTestProject.Weapons
{
    public class WeaponWithScope : Weapon
    {
        #region Serializable

        [SerializeField]
        private Camera camera;

        [SerializeField]
        private RenderTexture hiResTexture;

        [SerializeField]
        private RenderTexture lowResTexture;

        [SerializeField]
        private Renderer scope;

        #endregion

        private Material realMaterial;

        private bool userRealScope;

        public override void Init(int bulletsCount, bool isNpc)
        {
            userRealScope = !isNpc;
            base.Init(bulletsCount, isNpc);

            camera.gameObject.SetActive(userRealScope);

            if (userRealScope)
            {
                realMaterial = scope.material;
                camera.targetTexture = lowResTexture;
                realMaterial.SetTexture("_MainTex", lowResTexture);
            }
        }

        public override void OnAim()
        {
            if (userRealScope)
            {
                camera.targetTexture = hiResTexture;
                realMaterial.SetTexture("_MainTex", hiResTexture);
            }
        }

        public override void OnDeAim()
        {
            if (userRealScope)
            {
                camera.targetTexture = lowResTexture;
                realMaterial.SetTexture("_MainTex", lowResTexture);
            }
        }
    }
}
