﻿using System;
using FPSTestProject.Helpers.Runtime.SoundManager;
using UnityEngine;

namespace FPSTestProject.Characters.Animations
{
    public class FootstepSound : MonoBehaviour
    {
        #region Serializable

        [SerializeField]
        [Range(0, 5)]
        private float minDelay = 1;

        #endregion

        private float latPlayTime;
        
        private void OnTriggerEnter(Collider other)
        {
            if(Time.time - latPlayTime < minDelay)
                return;

            latPlayTime = Time.time;
            SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.Footstep), transform.position,
                0.25f, transform, false, 0.1f);
        }
    }
}
