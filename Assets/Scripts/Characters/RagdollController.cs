﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FPSTestProject.Assets.Scripts.Characters.Nps;
using FPSTestProject.Characters.Controllers;
using UnityEngine;

namespace FPSTestProject.Characters
{
    public class RagdollController : BaseCharacterComponent
    {
        private static readonly int upTime = Animator.StringToHash("GetUpTime");

        #region Serializable

        [SerializeField]
        [Range(0, 5)]
        private float getUpTime = 2f;

        [SerializeField]
        [Range(0, 5)]
        private float animationSpedMultiplyer = 2.8f / 2f;

        [SerializeField]
        [Range(0, 5)]
        private float ragdollToMechanimBlendTime = 0.5f;

        [SerializeField]
        [Range(0, 5)]
        private float mechanimToGetUpTransactionTime = 0.05f;

        [SerializeField]
        private Transform hips;

        #endregion

        public bool IsRagdolled
        {
            get => state != RagdollState.Animated;
            set
            {
                lastState = value;
                changeRagdoll = true;
            }
        }

        private bool changeRagdoll;
        private bool lastState;
        private Vector3 storedForce;

        private Animator animator;
        private NpcMovementController movementController;

        private RagdollState state;
        private float ragdollingEndTime = -100;

        private BodyPart[] bodyParts;
        private BodyPart hipsBodyPart;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);

            animator = characterHolder.GetCharacterComponent<CharacterAnimationController>().Animator;
            movementController = characterHolder.GetCharacterComponent<NpcMovementController>();

            animator.SetFloat(upTime, animationSpedMultiplyer);

            List<BodyPart> parts = new List<BodyPart>();
            CollectBodyPart(parts, hips);

            hipsBodyPart = parts.First();
            parts.Remove(hipsBodyPart);
            bodyParts = parts.ToArray();

            SetKinematic(true);

            StartCoroutine(InitFirstTimeRagdoll());
        }

        //TODO: Stange bug - ragdoll in first time is to laggy
        private IEnumerator InitFirstTimeRagdoll()
        {
            yield return new WaitForSeconds(0.25f);
            IsRagdolled = true;
            yield return new WaitForSeconds(0.25f);
            IsRagdolled = false;
        }

        void LateUpdate()
        {
            if (state != RagdollState.Blended)
                return;

            float ragdollBlendAmount = 1f -
                                        (Time.time - ragdollingEndTime - mechanimToGetUpTransactionTime) /
                                        ragdollToMechanimBlendTime;
            ragdollBlendAmount = Mathf.Clamp01(ragdollBlendAmount);

            if (ragdollBlendAmount >= 0.001)
            {
                hips.transform.position = Vector3.Lerp(hips.transform.position,
                    hipsBodyPart.storedPosition, ragdollBlendAmount);

                foreach (BodyPart part in bodyParts)
                {
                    part.transform.rotation = Quaternion.Slerp(part.transform.rotation, part.storedRotation,
                        ragdollBlendAmount);
                }
            }

            else if (Time.time - ragdollingEndTime >= getUpTime / animationSpedMultiplyer)
            {
                state = RagdollState.Animated;
            }
        }

        private void FixedUpdate()
        {
            if (changeRagdoll)
            {
                changeRagdoll = false;

                if (lastState && state != RagdollState.Ragdolled)
                {
                    SetKinematic(false);
                    state = RagdollState.Ragdolled;
                    hipsBodyPart.rigidbody.AddForce(storedForce);
                    storedForce = Vector3.zero;
                }
                else if (!lastState && state == RagdollState.Ragdolled)
                {
                    if (animator.GetBoneTransform(HumanBodyBones.Hips).forward.y > 0)
                    {
                        animator.SetTrigger("GetUpFromBack");
                        MoveRoot(true);
                    }
                    else
                    {
                        animator.SetTrigger("GetUpFromBelly");
                        MoveRoot(false);
                    }

                    state = RagdollState.Blended;
                    ragdollingEndTime = Time.time;
                    SetKinematic(true);

                    hipsBodyPart.storedPosition = hipsBodyPart.transform.position;
                    hipsBodyPart.storedRotation = hipsBodyPart.transform.rotation;

                    foreach (BodyPart bodyPart in bodyParts)
                    {
                        bodyPart.storedPosition = bodyPart.transform.position;
                        bodyPart.storedRotation = bodyPart.transform.rotation;
                    }
                }
            }
        }

        private void SetKinematic(bool isKinematic)
        {
            foreach (BodyPart part in bodyParts)
            {
                part.rigidbody.isKinematic = isKinematic;
                //for (int i = 0; i < part.colliders.Length; i++)
                //{
                //    part.colliders[i].enabled = !isKinematic;
                //}
            }

            hipsBodyPart.rigidbody.isKinematic = isKinematic;
            //for (int i = 0; i < hipsBodyPart.colliders.Length; i++)
            //{
            //    hipsBodyPart.colliders[i].enabled = !isKinematic;
            //}

            animator.enabled = isKinematic;
            movementController.IsLock = !isKinematic;
        }

        private void CollectBodyPart(List<BodyPart> parts, Transform nextTransform)
        {
            if (nextTransform.gameObject.layer == LayerMask.NameToLayer("Foot"))
                return;

            BodyPart part;
            if (CreateBodyPart(nextTransform, out part))
                parts.Add(part);

            for (int i = 0; i < nextTransform.childCount; i++)
            {
                CollectBodyPart(parts, nextTransform.GetChild(i));
            }
        }

        private bool CreateBodyPart(Transform partTransform, out BodyPart part)
        {
            var boneRigidbody = partTransform.GetComponent<Rigidbody>();
            if (boneRigidbody != null)
            {
                part = new BodyPart()
                {
                    transform = partTransform,
                    rigidbody = boneRigidbody,
                    colliders = partTransform.GetComponents<Collider>().Concat(partTransform.GetComponentsInChildren<Collider>()).ToArray()
                };
                return true;
            }
            part = null;
            return false;
        }

        private void MoveRoot(bool fromBack)
        {
            var horizontalDir = (fromBack ? -1 : 1) *
                                (animator.GetBoneTransform(HumanBodyBones.Head).position -
                                 0.5f *
                                 (animator.GetBoneTransform(HumanBodyBones.LeftToes).position +
                                  animator.GetBoneTransform(HumanBodyBones.RightToes).position)).normalized;

            var oldhipsPosition = hips.position;
            var oldhipsRotation = hips.rotation;

            movementController.transform.position = hips.position;
            movementController.transform.forward = -horizontalDir;

            hips.position = oldhipsPosition;
            hips.rotation = oldhipsRotation;
        }

        private enum RagdollState
        {
            Animated,
            Ragdolled,
            Blended
        }

        private class BodyPart
        {
            public Rigidbody rigidbody;
            public Transform transform;
            public Vector3 storedPosition;
            public Quaternion storedRotation;

            public Collider[] colliders;
        }

        #if UNITY_EDITOR

        [ContextMenu("SwitchRagdoll")]
        public void SwitchRagdoll()
        {
            IsRagdolled = !IsRagdolled;
        }

        #endif
        public void AddForce(Vector3 force)
        {
            if (IsRagdolled)
            {
                hipsBodyPart.rigidbody.AddForce(force);
            }
            else
            {
                storedForce += force;
            }
        }
    }
}
