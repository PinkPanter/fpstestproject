﻿using System;
using System.Collections;
using FPSTestProject.Characters.Controllers;
using FPSTestProject.Helpers.Runtime.Configs;
using UnityEngine;

namespace FPSTestProject.Characters
{
    public abstract class CharacterHolder : ConfigurableMonoBehaviour
    {
        public static CharacterHolder LocalPlayer;

        #region Serializable

        [SerializeField]
        [Configurable]
        [Range(0, 1000)]
        protected int health = 100;

        [SerializeField]
        [Range(0, 200)]
        protected int damageToPush = 100;

        [SerializeField]
        [Range(0, 10)]
        protected int standUpTime = 3;
        
        #endregion

        public event Action OnDead = delegate { }; 

        public event Action OnHealthChanged = delegate { }; 

        public int CurrentHealth { get; protected set; }

        public int MaxHealth => health;

        public bool IsDead => CurrentHealth <= 0;

        public Vector3 Position => transform.position + new Vector3(0, 1.4f, 0);

        private BaseCharacterComponent[] characterComponents;

        protected override void Awake()
        {
            base.Awake();

            CurrentHealth = health;

            characterComponents = GetComponentsInChildren<BaseCharacterComponent>();

            foreach (var characterComponent in characterComponents)
            {
                characterComponent.Init(this);
            }
        }

        public BaseCharacterComponent GetCharacterComponent(Type type)
        {
            foreach (var characterComponent in characterComponents)
            {
                if (characterComponent.GetType() == type || characterComponent.GetType().IsSubclassOf(type))
                {
                    return characterComponent;
                }
            }

            return null;
        }

        public T GetCharacterComponent<T>() where T : BaseCharacterComponent
        {
            return (T)GetCharacterComponent(typeof(T));
        }

        public void Damage(int damage)
        {
            if (IsDead)
                return;

            CurrentHealth -= damage;

            if (CurrentHealth <= 0)
                DoDeath();

            OnHealthChanged();
        }

        public void DamageAndPush(int damage, Vector3 force)
        {
            bool wasDead = IsDead;

            CurrentHealth -= damage;
            if (CurrentHealth <= 0 && !wasDead)
            {
                DoDeath();
            }

            if (damage >= damageToPush || IsDead)
            {
                StartCoroutine(EnableRagdoll(force));
            }

            OnHealthChanged();
        }

        public void Heal(int healingEffect)
        {
            if(IsDead)
                return;

            CurrentHealth = Mathf.Clamp(CurrentHealth + healingEffect, 1, health);
            OnHealthChanged();
        }

        protected virtual void DoDeath()
        {
            OnDead();

            var ragdoll = GetCharacterComponent<RagdollController>();
            if (ragdoll != null)
            {
                ragdoll.IsRagdolled = true;
                StartCoroutine(WaitForDestroy());
            }

            GetCharacterComponent<WeaponController>().DropWeapon();
        }

        private IEnumerator WaitForDestroy()
        {
            yield return new WaitForSeconds(10);
            var anyRenderer = GetComponentInChildren<Renderer>();
            int counter = 0;

            while (anyRenderer.isVisible && counter < 10)
            {
                yield return new WaitForSeconds(1);
                counter++;
            }

            Destroy(gameObject);
        }

        private IEnumerator EnableRagdoll(Vector3 force)
        {
            var ragdoll = GetCharacterComponent<RagdollController>();
            if (ragdoll != null)
            {
                ragdoll.IsRagdolled = true;
                ragdoll.AddForce(force);
                yield return new WaitForSeconds(standUpTime);
                if (!IsDead)
                    ragdoll.IsRagdolled = false;
            }
        }
    }
}
