﻿using FPSTestProject.Characters;
using FPSTestProject.Characters.Controllers;
using UnityEngine;
using UnityEngine.AI;

namespace FPSTestProject.Assets.Scripts.Characters.Nps
{
    public class NpcMovementController : MovementController
    {
        #region Serializable

        [SerializeField]
        private NavMeshAgent navMeshAgent;

        #endregion

        public override Vector3 Velocity => navMeshAgent.velocity;

        private Vector3 realVelocity;
        private Vector3 prevPosition;

        public override bool IsLock
        {
            get => isLock;
            set
            {
                isLock = value;
                navMeshAgent.enabled = !isLock;
            }
        }

        protected override void Update()
        {
            navMeshAgent.speed = isSprint ? sprintSpeed : speed;

            base.Update();
        }

        public void MoveTo(Vector3 target)
        {
            navMeshAgent.isStopped = false;
            navMeshAgent.SetDestination(target);
        }

        public void SetSprint(bool sprint)
        {
            isSprint = sprint;
        }

        public void Stop()
        {
            navMeshAgent.isStopped = true;
        }

        public void EnableAutoRotation(bool isEnabled)
        {
            navMeshAgent.updateRotation = isEnabled;
        }
    }
}
