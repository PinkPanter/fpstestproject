﻿using System;
using FPSTestProject.Characters;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FPSTestProject.Characters.Nps
{
    public class NpcCharacter : CharacterHolder
    {
        #region Serializable

        [SerializeField]
        private bool overrideConfigs;

        [SerializeField]
        private int newHealth;

        [SerializeField]
        private Loot[] loots;

        #endregion

        protected override void Awake()
        {
            base.Awake();

            if (overrideConfigs)
            {
                health = newHealth;
                CurrentHealth = newHealth;
            }
        }

        protected override void DoDeath()
        {
            base.DoDeath();

            foreach (Loot loot in loots)
            {
                if (Random.value < loot.chance)
                {
                    Instantiate(loot.prefab, Position + Random.insideUnitSphere, Quaternion.identity);
                }
            }
        }

        [Serializable]
        private struct Loot
        {
            public GameObject prefab;
            public float chance;
        }
    }
}
