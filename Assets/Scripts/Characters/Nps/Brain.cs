﻿using System;
using System.Collections;
using FPSTestProject.Assets.Scripts.Characters.Nps;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FPSTestProject.Characters.Nps
{
    public class Brain : BaseCharacterComponent
    {
        #region Serializable

        [SerializeField] private LayerMask obstacles;

        [SerializeField] private float closestPoint = 15;

        [SerializeField] private Transform bodyRot;

        [SerializeField] private float chanceToShootWhileRun = 0.25f;

        [SerializeField] private float accuracy = 25f;

        #endregion

        private NpcMovementController npsMovementController;
        private RagdollController ragdollController;
        private NpcWeaponController weaponController;

        private CharacterHolder localPlayer;
        private Vector3 lastPlayerPosition;

        private bool doRealAim = false;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);

            npsMovementController = characterHolder.GetCharacterComponent<NpcMovementController>();
            ragdollController = characterHolder.GetCharacterComponent<RagdollController>();
            weaponController = characterHolder.GetCharacterComponent<NpcWeaponController>();

            localPlayer = CharacterHolder.LocalPlayer;

            StartCoroutine(Decision());
        }

        private IEnumerator Decision()
        {
            while (true)
            {
                if (characterHolder.IsDead)
                    yield break;

                yield return new WaitForSeconds(0.25f);

                if (ragdollController.IsRagdolled || Time.timeScale <= 0.01f)
                    continue;

                if (localPlayer == null)
                {
                    localPlayer = CharacterHolder.LocalPlayer;
                    if (localPlayer == null)
                        continue;
                }

                if (localPlayer.IsDead)
                    yield break;

                bool isPlayerSeen = IsPlayerSeen();
                var currentDistance = Vector3.Distance(localPlayer.transform.position, transform.position);

                if (currentDistance < closestPoint && isPlayerSeen)
                {
                    npsMovementController.Stop();
                    Aim();
                }
                else if (Vector3.Distance(lastPlayerPosition, localPlayer.transform.position) >= 1)
                {
                    lastPlayerPosition = localPlayer.transform.position;
                    npsMovementController.MoveTo(lastPlayerPosition);
                }
                else
                {
                    if (!isPlayerSeen)
                    {
                        doRealAim = false;
                        npsMovementController.EnableAutoRotation(true);
                    }
                    else
                    {
                        Aim();
                        if (Random.value <= chanceToShootWhileRun)
                        {
                            var test = ((localPlayer.Position - weaponController.CurrentWeapon.ShootPos).normalized
                            + weaponController.CurrentWeapon.InverseDirectionByShootPoint(
                                Random.insideUnitCircle * accuracy)).normalized;
                            weaponController.CurrentWeapon.FakeShootDir = test;
                            Debug.DrawRay(weaponController.CurrentWeapon.ShootPos, test, Color.red, 10);

                            weaponController.Fire();
                        }
                    }
                }
            }
        }

        private void Aim()
        {
            npsMovementController.EnableAutoRotation(false);

            transform.eulerAngles = new Vector3(0,
                Quaternion.LookRotation((localPlayer.transform.position - transform.position).normalized).eulerAngles.y,
                0);

            doRealAim = true;
        }

        private void LateUpdate()
        {
            if (characterHolder.IsDead || ragdollController.IsRagdolled || Time.timeScale <= 0.01f)
                return;

            if (doRealAim)
            {
                var dirToShoot = (localPlayer.Position - weaponController.CurrentWeapon.ShootPos).normalized;
                var dirToMove = Quaternion.FromToRotation(weaponController.CurrentWeapon.ShootDir, dirToShoot)
                    .eulerAngles.y;

                if (dirToMove >= 50)
                    return;

                var currentEuler = bodyRot.localEulerAngles;
                currentEuler.x -= dirToMove * 0.66f;
                bodyRot.localEulerAngles = currentEuler;

                var test = ((localPlayer.Position - weaponController.CurrentWeapon.ShootPos).normalized
                + weaponController.CurrentWeapon.InverseDirectionByShootPoint(
                    Random.insideUnitCircle * accuracy)).normalized;
                weaponController.CurrentWeapon.FakeShootDir = test;
                Debug.DrawRay(weaponController.CurrentWeapon.ShootPos, test, Color.red, 10);

                weaponController.Fire();
            }
        }

        private bool IsPlayerSeen()
        {
            return !Physics.Linecast(characterHolder.Position, localPlayer.Position, obstacles);
        }
    }
}
