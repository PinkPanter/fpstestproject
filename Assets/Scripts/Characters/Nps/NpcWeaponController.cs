﻿using FPSTestProject.Characters;
using FPSTestProject.Characters.Controllers;
using FPSTestProject.Weapons;

namespace FPSTestProject.Assets.Scripts.Characters.Nps
{
    public class NpcWeaponController : WeaponController
    {
        protected override bool ReceiveFromFirst => false;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);

            foreach (Weapon weapon in availableWeapons)
            {
                weapon.Init(99999, true);
            }
        }

        public override void Fire()
        {
            if (currentWeapon.CurrentClipCount <= 0)
            {
                Reload();
                return;
            }

            base.Fire();
        }
    }
}
