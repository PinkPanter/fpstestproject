﻿using UnityEngine;

namespace FPSTestProject.Characters.Controllers
{
    public class CharacterAnimationController : BaseCharacterComponent
    {
        private static int VelYFloat = Animator.StringToHash("VelY");
        private static int VelXFloat = Animator.StringToHash("VelX");
        private static int WeaponTypeInt = Animator.StringToHash("WeaponType");
        private static int EquipTrigger = Animator.StringToHash("Equip");
        private static int FireTrigger = Animator.StringToHash("Shoot");
        private static int JumpBool = Animator.StringToHash("IsJump");
        private static int ReloadTrigger = Animator.StringToHash("Reload");
        private static int ThrowTrigger = Animator.StringToHash("Throw");
        private static int SpeedModFloat = Animator.StringToHash("SpeedMod");
        private static int LeftBulletsInt = Animator.StringToHash("LeftBullets");

        private const float animToUnits = 4f;

        #region Serializable

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private Animator firstPersonAnimator;

        [SerializeField]
        private LayerMask ikMask;

        #endregion

        public Animator Animator => animator;

        private MovementController movementController;

        private RagdollController ragdollController;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);
            movementController = characterHolder.GetCharacterComponent<MovementController>();
            ragdollController = characterHolder.GetCharacterComponent<RagdollController>();
        }

        private void Update()
        {
            if (Time.timeScale < 0.01f || characterHolder != null && characterHolder.IsDead || (ragdollController != null && ragdollController.IsRagdolled))
                return;

            var nextVelocity = movementController.SmootherVelocity;

            bool isJump = !movementController.IsGrounded();

            animator.SetBool(JumpBool, isJump);

            animator.SetFloat(SpeedModFloat, movementController.IsSprint ? movementController.SprintSpeed / 8 : movementController.Speed / 4);

            animator.SetFloat(VelYFloat, (nextVelocity.z / animToUnits));
            animator.SetFloat(VelXFloat, (nextVelocity.x / animToUnits));
        }

        public void EquipWeapon(RuntimeAnimatorController currentWeaponAnimatorController, int currentWeaponAnimType)
        {
            //TODO: Create Init and Initialize
            if (characterHolder != null && characterHolder.IsDead)
                return;

            if (firstPersonAnimator != null)
            {
                firstPersonAnimator.runtimeAnimatorController = currentWeaponAnimatorController;
                firstPersonAnimator.SetTrigger(EquipTrigger);
            }

            animator.SetInteger(WeaponTypeInt, currentWeaponAnimType);
            animator.SetTrigger(EquipTrigger);
        }

        public void Fire()
        {
            animator.SetTrigger(FireTrigger);

            if (firstPersonAnimator != null)
                firstPersonAnimator.SetTrigger(FireTrigger);
        }

        public void Reload(int leftBullets)
        {
            animator.SetTrigger(ReloadTrigger);

            if (firstPersonAnimator != null)
            {
                firstPersonAnimator.SetInteger(LeftBulletsInt, leftBullets);
                firstPersonAnimator.SetTrigger(ReloadTrigger);
            }
        }

        public void ThrowGrenade()
        {
            animator.SetLayerWeight(2, 1);
            animator.SetTrigger(ThrowTrigger);

            if (firstPersonAnimator != null)
            {
                firstPersonAnimator.SetLayerWeight(1, 1);
                firstPersonAnimator.SetTrigger(ThrowTrigger);
            }
        }

        public void StopThrow()
        {
            animator.SetLayerWeight(2, 0);

            if (firstPersonAnimator != null)
                firstPersonAnimator.SetLayerWeight(1, 0);
        }

        private void OnAnimatorIK(int layerIndex)
        {
            if (characterHolder != null && characterHolder.IsDead || (ragdollController != null && ragdollController.IsRagdolled))
                return;

            SetFoot(AvatarIKGoal.LeftFoot);
            SetFoot(AvatarIKGoal.RightFoot);
        }

        private void SetFoot(AvatarIKGoal goal)
        {
            //TODO: Only after foot step
            var currentPos = animator.GetIKPosition(goal);
            RaycastHit hit;
            if (Physics.Raycast(currentPos, -Vector3.up, out hit, 1, ikMask))
            {
                if (hit.point.y + 0.1f > currentPos.y)
                {
                    animator.SetIKPositionWeight(goal, 1);
                    animator.SetIKPosition(goal, hit.point + new Vector3(0, 0.1f));
                    return;
                }
            }

            animator.SetIKPositionWeight(goal, 0);
        }
    }
}
