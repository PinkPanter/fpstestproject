﻿using FPSTestProject.Helpers.Runtime.Configs;
using UnityEngine;

namespace FPSTestProject.Characters.Controllers
{
    public abstract class MovementController : BaseCharacterComponent
    {
        #region Serializable
        
        [SerializeField]
        [Configurable(displayName = "Character speed")]
        [Range(0, 10)]
        protected float speed = 4;

        [SerializeField]
        [Configurable(displayName = "Character sprint speed")]
        [Range(0, 10)]
        protected float sprintSpeed = 6;

        #endregion
        public float Speed => speed;

        public float SprintSpeed => sprintSpeed;

        public abstract Vector3 Velocity { get; }

        public Vector3 SmootherVelocity => smootherVelocity;

        public abstract bool IsLock { get; set; }

        public bool IsSprint => isSprint;

        protected bool isSprint;
        protected Vector3 movementDirection;
        protected Vector3 lookingDirection;
        
        protected bool isLock;

        protected Vector3 smootherVelocity;

        private RaycastHit[] hitResults = new RaycastHit[1];
        private int hitResultsRelatesToFrame;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);
            lookingDirection = transform.forward;
        }

        protected virtual void Update()
        {
            smootherVelocity = Vector3.Lerp(smootherVelocity, transform.InverseTransformDirection(Velocity), Time.smoothDeltaTime * 15f);
        }

        public bool IsGrounded()
        {
            if (Time.frameCount == hitResultsRelatesToFrame)
                return hitResults[0].collider;

            hitResults[0] = default;

            Physics.RaycastNonAlloc(new Ray(transform.position, -Vector3.up), hitResults, 0.3f);
            hitResultsRelatesToFrame = Time.frameCount;

            return (hitResults[0].collider != null);
        }
    }
}
