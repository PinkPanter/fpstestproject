﻿using System.Collections.Generic;
using FPSTestProject.Helpers.Runtime.ObjectPool;
using FPSTestProject.Helpers.Runtime.SoundManager;
using FPSTestProject.Weapons;
using FPSTestProject.Weapons.Pool;
using FPSTestProject.Weapons.Pool.GrenadePools;
using UnityEngine;

namespace FPSTestProject.Characters.Controllers
{
    public abstract class WeaponController : BaseCharacterComponent
    {
        #region Serializable

        [SerializeField]
        protected Weapon[] weaponsToAddOnStart;

        [SerializeField]
        protected Transform weaponHolder;

        [SerializeField]
        protected int grenadeInitialCount = 3;

        [SerializeField]
        protected GameObject grenadeHolder;

        [SerializeField]
        protected string weaponLayer;

        #endregion

        public Weapon CurrentWeapon => currentWeapon;

        public int GrenatesCount => grenadeCount;

        protected abstract bool ReceiveFromFirst { get; }

        protected Weapon currentWeapon;

        protected List<Weapon> availableWeapons;

        protected CharacterAnimationController animationController;
        protected MovementController movementController;

        protected int grenadeCount;
        protected bool isThrowingGrenade;

        protected bool isReloading;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);
            animationController = characterHolder.GetCharacterComponent<CharacterAnimationController>();
            movementController = characterHolder.GetCharacterComponent<MovementController>();

            availableWeapons = new List<Weapon>();

            for (var i = 0; i < weaponsToAddOnStart.Length; i++)
            {
                var additionalWeapon =
                    Instantiate(weaponsToAddOnStart[i].gameObject, weaponHolder).GetComponent<Weapon>();
               availableWeapons.Add(additionalWeapon);
               additionalWeapon.transform.localScale = Vector3.one;
               additionalWeapon.gameObject.SetActive(false);
            }

            grenadeCount = grenadeInitialCount;

            EquipCurrentWeapon(availableWeapons[0]);
        }

        public virtual void Fire()
        {
            if (characterHolder.IsDead)
                return;

            if (currentWeapon.Fire())
            {
                animationController.Fire();
            }
        }

        public void Reload()
        {
            if (characterHolder.IsDead || isReloading)
                return;

            if (currentWeapon.Reload())
            {
                isReloading = true;
                animationController.Reload(currentWeapon.CurrentClipCount);
            }
        }

        public void Aim(bool isAim)
        {
            if (characterHolder.IsDead || currentWeapon.IsLock)
                return;

            if(isAim)
                currentWeapon.OnAim();
            else
                currentWeapon.OnDeAim();

        }

        public void ThrowGrenade()
        {
            if (characterHolder.IsDead || isReloading)
                return;

            if (grenadeCount > 0 && !isThrowingGrenade)
            {
                SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.GrenadeThrow),
                    transform.position, 0.75f, transform);
                grenadeCount--;
                grenadeHolder.gameObject.SetActive(true);
                animationController.ThrowGrenade();
                isThrowingGrenade = true;
            }
        }

        public void LockWeapon()
        {
            currentWeapon.IsLock = true;
        }

        public void UnlockWeapon()
        {
            currentWeapon.IsLock = false;
        }

        #region AnimationCallbacks

        public virtual void ReloadEnd(int firstAnim)
        {
            if(ReceiveFromFirst != (firstAnim == 1))
                return;

            isReloading = false;

            currentWeapon.ReloadEnd();
        }

        public virtual void EquipEnd(int firstAnim)
        {
            isReloading = false;
            if (ReceiveFromFirst != (firstAnim == 1))
                return;
        }

        public virtual void GrenadeRealThrow(int firstAnim)
        {
            if (ReceiveFromFirst != (firstAnim == 1))
                return;

            grenadeHolder.gameObject.SetActive(false);
            var realGrenade = PoolInitializer.GetPool<Grenade>()
                .GetObject(grenadeHolder.transform.position, grenadeHolder.transform.rotation);
            realGrenade.Throw(grenadeHolder.transform.forward);
        }

        public virtual void EndThrow(int firstAnim)
        {
            if (ReceiveFromFirst != (firstAnim == 1))
                return;

            animationController.StopThrow();
            isThrowingGrenade = false;
        }

        #endregion

        protected virtual void EquipCurrentWeapon(Weapon newCurrentWeapon)
        {
            if(isThrowingGrenade)
                return;

            isReloading = false;
            if (currentWeapon != null)
                currentWeapon.Deequip();

            currentWeapon = newCurrentWeapon;
            animationController.EquipWeapon(currentWeapon.AnimatorController, currentWeapon.AnimType);
            currentWeapon.Equip();

            SetLayerToObjectAndChilds(currentWeapon.transform, LayerMask.NameToLayer(weaponLayer));
        }

        private static void SetLayerToObjectAndChilds(Transform activeTransform, int layer)
        {
            activeTransform.gameObject.layer = layer;
            foreach (Transform childs in activeTransform)
            {
                SetLayerToObjectAndChilds(childs, layer);
            }
        }

        public void DropWeapon()
        {
            currentWeapon.TransformToItem();
        }
    }
}
