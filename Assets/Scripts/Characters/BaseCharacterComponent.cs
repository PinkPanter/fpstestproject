﻿using FPSTestProject.Helpers.Runtime.Configs;
using UnityEngine;

namespace FPSTestProject.Characters
{
    public abstract class BaseCharacterComponent : ConfigurableMonoBehaviour
    {
        protected CharacterHolder characterHolder;

        public virtual void Init(CharacterHolder characterHolder)
        {
            this.characterHolder = characterHolder;
        }
    }
}
