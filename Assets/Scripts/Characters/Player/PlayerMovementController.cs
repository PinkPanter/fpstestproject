﻿using FPSTestProject.Characters.Controllers;
using UnityEngine;

namespace FPSTestProject.Characters.Player
{
    public class PlayerMovementController : MovementController
    {
        #region Serializable

        [SerializeField]
        protected CharacterController characterController;

        [SerializeField]
        [Range(0, 80)]
        private float sencivity = 20;

        [SerializeField]
        protected AnimationCurve jumpForce;

        #endregion
        public override bool IsLock
        {
            get => isLock;
            set
            {
                isLock = value;
                characterController.enabled = !isLock;
            }
        }

        public override Vector3 Velocity => transform.InverseTransformDirection(characterController.velocity);

        private bool isJumpNow;
        private float lastJumpTime;

        protected override void Update()
        {
            if (IsLock || !Application.isFocused || Time.timeScale <= 0.01f || characterHolder.IsDead)
                return;

            movementDirection.x = Input.GetAxis("Horizontal");
            movementDirection.z = Input.GetAxis("Vertical");
            movementDirection.y = 0;
            movementDirection.Normalize();

            var currentRot = transform.eulerAngles;
            currentRot.y += Input.GetAxis("Mouse X") * Time.deltaTime * sencivity;
            lookingDirection = Quaternion.Euler(currentRot) * Vector3.forward;

            isSprint = Input.GetButton("Sprint");
            var isJump = Input.GetKeyDown(KeyCode.Space);
            transform.forward = lookingDirection;

            movementDirection *= isSprint ? sprintSpeed : speed;
            movementDirection = transform.TransformDirection(movementDirection);
            movementDirection.y = Physics.gravity.y;

            if (isJumpNow)
            {
                movementDirection.y += jumpForce.Evaluate(Time.time - lastJumpTime);
                if (Time.time - lastJumpTime >= jumpForce.keys[jumpForce.length - 1].time)
                    isJumpNow = false;
            }
            //else if (characterController.isGrounded) : NOT RELIABLE!
            else if (IsGrounded())
            {
                if (isJump)
                {
                    isJumpNow = true;
                    lastJumpTime = Time.time;
                }
            }

            characterController.Move(movementDirection * Time.deltaTime);

            base.Update();
        }
    }
}
