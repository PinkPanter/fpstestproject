﻿using System.Collections;
using FPSTestProject.Visual;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace FPSTestProject.Characters.Player
{
    public class PlayerCameraController : BaseCharacterComponent
    {
        #region Serializable

        [SerializeField]
        [Range(0, 180)]
        private float recoilResist = 80;

        [SerializeField]
        [Range(0, 80)]
        private float sencivity = 50;

        [SerializeField]
        [Range(0, 180)]
        private float downMaxAngle = 90;
        [SerializeField]
        [Range(0, 180)]
        private float upMaxAngle = 90;

        [SerializeField]
        private PostProcessVolume postProcessVolume;

        [SerializeField]
        [Range(0, 100)]
        private float damageForFullEffect = 100;

        [SerializeField]
        [Range(0, 50)]
        private float bloodSpeed = 15;

        #endregion

        private BloodMask bloodMask;

        private float additionalRecoil;

        private int lastHealth;

        private float currentEffectValue;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);

            lastHealth = characterHolder.CurrentHealth;

            postProcessVolume.profile.TryGetSettings(out bloodMask);

            characterHolder.GetCharacterComponent<PlayerWeaponController>().OnWeaponShoot += OnOnWeaponShoot;
            characterHolder.OnDead += CharacterHolderOnOnDead;
            characterHolder.OnHealthChanged += CharacterHolderOnHealthChanged;
        }

        private void CharacterHolderOnHealthChanged()
        {
            if (lastHealth > characterHolder.CurrentHealth)
            {
                currentEffectValue = Mathf.Clamp01(currentEffectValue + (lastHealth - characterHolder.CurrentHealth) / damageForFullEffect);
                lastHealth = characterHolder.CurrentHealth;
            }
        }
        
        private void CharacterHolderOnOnDead()
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }

        private void OnOnWeaponShoot(float recoil)
        {
            additionalRecoil = recoil / 3f;
        }

        private void Update()
        {
            if (characterHolder.IsDead)
            {
                bloodMask.enabled.value = true;
                bloodMask.opacity.value = Mathf.Lerp(bloodMask.opacity.value, 1, Time.deltaTime * bloodSpeed);
                return;
            }
            else
            {
                currentEffectValue = Mathf.Lerp(currentEffectValue, 0, Time.deltaTime * bloodSpeed / 2f);
                if (currentEffectValue <= 0.01f)
                {
                    bloodMask.enabled.value = false;
                    bloodMask.opacity.value = 0;
                }
                else
                {
                    bloodMask.enabled.value = true;
                    bloodMask.opacity.value = Mathf.Lerp(bloodMask.opacity.value, currentEffectValue, Time.deltaTime * bloodSpeed);
                }
            }

            if (!Application.isFocused || Time.timeScale <= 0.01f)
                return;

            var currentRot = transform.eulerAngles;
            currentRot.x -= Input.GetAxis("Mouse Y") * Time.deltaTime * sencivity + additionalRecoil;
            additionalRecoil = Mathf.Lerp(additionalRecoil, 0, Time.deltaTime * recoilResist);
            if (additionalRecoil <= 0.5f)
                additionalRecoil = 0;

            if (currentRot.x > 180)
            {
                currentRot.x = Mathf.Clamp(currentRot.x, 360 - upMaxAngle, 360);
            }
            else
            {
                currentRot.x = Mathf.Clamp(currentRot.x, -1, downMaxAngle);
            }

            transform.eulerAngles = currentRot;

            #if UNITY_EDITOR
            if(Input.GetKeyDown(KeyCode.Pause))
                Debug.Break();
#endif
        }
    }
}
