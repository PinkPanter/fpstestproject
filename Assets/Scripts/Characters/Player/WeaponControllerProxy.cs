﻿namespace FPSTestProject.Characters.Player
{
    public class WeaponControllerProxy : BaseCharacterComponent
    {
        private PlayerWeaponController playerWeaponController;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);
            playerWeaponController = characterHolder.GetCharacterComponent<PlayerWeaponController>();
        }

        #region AnimationCallbacks

        public void ReloadEnd(int firstAnim)
        {
            playerWeaponController.ReloadEnd(firstAnim);
        }

        public virtual void EquipEnd(int firstAnim)
        {
            playerWeaponController.EquipEnd(firstAnim);
        }

        public virtual void GrenadeRealThrow(int firstAnim)
        {
            playerWeaponController.GrenadeRealThrow(firstAnim);
        }

        public virtual void EndThrow(int firstAnim)
        {
            playerWeaponController.EndThrow(firstAnim);
        }

        #endregion
    }
}
