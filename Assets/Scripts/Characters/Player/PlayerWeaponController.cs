﻿using System;
using FPSTestProject.Characters.Controllers;
using FPSTestProject.Helpers.Runtime.SoundManager;
using FPSTestProject.UI.Objects;
using FPSTestProject.Weapons;
using UnityEngine;

namespace FPSTestProject.Characters.Player
{
    public class PlayerWeaponController : WeaponController
    {
        public event Action<float> OnWeaponShoot = delegate { };
        public event Action<Weapon> OnWeaponChanged = delegate { };
        public event Action<Weapon> OnWeaponBulletsChanged = delegate { };
        public event Action<int> OnGrenadesChanged = delegate { };

        #region Serializable

        [SerializeField]
        private Transform handTransform;

        [SerializeField]
        [Range(0, 2)]
        private float aimTime = 0.5f;

        [SerializeField]
        [Range(0, 50)]
        private float handShake = 15f;
        [SerializeField]
        [Range(0, 50)]
        private float yHandShake = 5f;
        [SerializeField]
        [Range(0, 50)]
        private float xHandShake = 2f;

        #endregion

        protected override bool ReceiveFromFirst => true;

        private Vector3 defHandPos;
        private Vector3 aimPos;
        private bool isAim;
        private bool isAimAnim;

        private float aimAnimState;

        private Quaternion additionaShift = Quaternion.identity;

        public override void Init(CharacterHolder characterHolder)
        {
            base.Init(characterHolder);

            foreach (Weapon weapon in availableWeapons)
            {
                weapon.Init(weapon.ClipCount * 2, false);
            }
        }
        
        public void AddWeapon(Weapon weapon)
        {
            for (int i = 0; i < availableWeapons.Count; i++)
            {
                if (availableWeapons[i].AnimType == weapon.AnimType)
                {
                    availableWeapons[i].TotalBullets += (int)(weapon.ClipCount / 2f);
                    SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.Collect), transform.position,
                        0.75f);
                    Destroy(weapon.gameObject);

                    if (availableWeapons[i] == currentWeapon)
                        OnWeaponBulletsChanged(currentWeapon);
                    return;
                }
            }

            weapon.TransformToUsagable();
            weapon.transform.SetParent(weaponHolder);
            weapon.transform.localScale = Vector3.one;

            availableWeapons.Add(weapon);
            availableWeapons[availableWeapons.Count - 1].Init(weapon.ClipCount, false);

            EquipCurrentWeapon(availableWeapons[availableWeapons.Count - 1]);
        }

        public void AddGrenade(int count)
        {
            grenadeCount += count;
            SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.Collect), transform.position,
                0.75f);
            OnGrenadesChanged(grenadeCount);
        }


        protected override void EquipCurrentWeapon(Weapon newCurrentWeapon)
        {
            additionaShift = Quaternion.Euler(45, 0, 0);
            if(currentWeapon != null)
                currentWeapon.OnWeaponShoot -= CurrentWeaponOnOnWeaponShoot;

            base.EquipCurrentWeapon(newCurrentWeapon);
            handTransform.localPosition = currentWeapon.HandShift;
            defHandPos = currentWeapon.HandShift;

            handTransform.localEulerAngles = currentWeapon.HandRot;

            currentWeapon.OnWeaponShoot += CurrentWeaponOnOnWeaponShoot;

            OnWeaponChanged(currentWeapon);
        }

        public override void GrenadeRealThrow(int firstAnim)
        {
            base.GrenadeRealThrow(firstAnim);
            OnGrenadesChanged(grenadeCount);
        }

        public override void ReloadEnd(int firstAnim)
        {
            base.ReloadEnd(firstAnim);
            OnWeaponBulletsChanged(currentWeapon);
        }

        private void Update()
        {
            if(characterHolder.IsDead || Time.timeScale <= 0.01f)
                return;

            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                for (int i = 0; i < availableWeapons.Count; i++)
                {
                    if (availableWeapons[i] == currentWeapon)
                    {
                        i++;
                        if (i == availableWeapons.Count)
                            i = 0;

                        if(availableWeapons[i] != currentWeapon)
                            EquipCurrentWeapon(availableWeapons[i]);

                        return;
                    }
                }
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {

                for (int i = availableWeapons.Count - 1; i >= 0; i--)
                {
                    if (availableWeapons[i] == currentWeapon)
                    {
                        i--;
                        if (i < 0)
                            i = availableWeapons.Count - 1;

                        if (availableWeapons[i] != currentWeapon)
                            EquipCurrentWeapon(availableWeapons[i]);

                        return;
                    }
                }
            }

            if (Input.GetButtonDown("Fire2") && !currentWeapon.IsLock)
            {
                Sign.Instance.IsVisible = false;
                handTransform.localRotation = Quaternion.identity;
                aimPos = defHandPos - handTransform.parent.InverseTransformPoint(currentWeapon.AimPos); 

                Aim(true);
                isAim = true;
                isAimAnim = true;
            }
            else if (Input.GetButtonUp("Fire2") && !currentWeapon.IsLock)
            {
                Sign.Instance.IsVisible = true;
                Aim(false);
                isAim = false;
                isAimAnim = true;
            }

            if (Input.GetButtonDown("Reload"))
                Reload();

            if (Input.GetButtonDown("Grenade"))
                ThrowGrenade();

            if (currentWeapon.Automatic)
            {
                if(Input.GetButton("Fire1"))
                    Fire();
            }
            else
            {
                if (Input.GetButtonDown("Fire1"))
                    Fire();
            }

            MoveArms();
        }

        private void MoveArms()
        {
            var velocity = Mathf.Clamp(movementController.SmootherVelocity.magnitude, 1, 10);
            additionaShift = Quaternion.Lerp(additionaShift, Quaternion.identity, Time.smoothDeltaTime * 15f);
            Quaternion initialAngle = Quaternion.Euler(isAim ? Vector3.zero : currentWeapon.HandRot);

            handTransform.localRotation = Quaternion.Lerp(
                    Quaternion.Euler(Mathf.Sin(Time.time * xHandShake * (isAim ? 0.75f : 1)) * velocity * (isAim ? 0.25f : 1),
                        Mathf.Cos(Time.time * yHandShake * (isAim ? 0.75f : 1)) * velocity * (isAim ? 0.25f : 1),
                        0), initialAngle * additionaShift, Time.smoothDeltaTime * handShake);
            
            if (isAimAnim)
            {
                aimAnimState += (isAim ? 1 : -1) * Time.deltaTime;

                if (aimAnimState <= 0)
                {
                    isAimAnim = false;
                    aimAnimState = 0;
                    handTransform.localPosition = defHandPos;
                    return;
                }
                if (aimAnimState >= aimTime)
                {
                    isAimAnim = false;
                    aimAnimState = aimTime;
                    handTransform.localPosition = aimPos;
                    return;
                }

                handTransform
                    .localPosition =
                    Vector3.Lerp(defHandPos, aimPos, aimAnimState / aimTime);
            }
        }

        private void CurrentWeaponOnOnWeaponShoot()
        {
            additionaShift = Quaternion.Euler(-currentWeapon.Recoil * (isAim ? 1 : 2), 0, 0);
            OnWeaponShoot(currentWeapon.Recoil);
            OnWeaponBulletsChanged(currentWeapon);
        }
    }
}
