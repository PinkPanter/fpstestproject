﻿namespace FPSTestProject.Characters
{
    public class PlayerCharacter : CharacterHolder
    {
        protected override void Awake()
        {
            base.Awake();
            LocalPlayer = this;
        }
    }
}
