using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace FPSTestProject.Visual
{
    /// <summary>
    /// This class holds settings for the BloodMask effect.
    /// </summary>
    [Serializable]
    [PostProcess(typeof(BloodMaskRenderer), PostProcessEvent.AfterStack, "Unity/BloodMask")]
    public class BloodMask : PostProcessEffectSettings
    {
        /// <summary>
        /// The color to use to tint the BloodMask.
        /// </summary>
        [Tooltip("BloodMask color.")]
        public ColorParameter color = new ColorParameter { value = new Color(1f, 0f, 0f, 1f) };
        
        /// <summary>
        /// A black and white mask to use as a BloodMask.
        /// </summary>
        [Tooltip("A black and white mask to use as a BloodMask.")]
        public TextureParameter mask = new TextureParameter { value = null };

        /// <summary>
        /// Mask opacity.
        /// </summary>
        [Range(0f, 1f), Tooltip("Mask opacity.")]
        public FloatParameter opacity = new FloatParameter { value = 1f };

        /// <inheritdoc />
        public override bool IsEnabledAndSupported(PostProcessRenderContext context)
        {
            return enabled.value && opacity.value > 0f && mask.value != null;
        }
    }

#if UNITY_2017_1_OR_NEWER
    [UnityEngine.Scripting.Preserve]
#endif
    public class BloodMaskRenderer : PostProcessEffectRenderer<BloodMask>
    {
        private static readonly int color = Shader.PropertyToID("_Color");
        private static readonly int opacity = Shader.PropertyToID("_Opacity");
        private static readonly int mask = Shader.PropertyToID("_Mask");

        public override void Render(PostProcessRenderContext context)
        {
            var sheet = context.propertySheets.Get(Shader.Find("Custom/PP/BloodMask"));

            sheet.properties.SetColor(color, settings.color);
            sheet.properties.SetFloat(opacity, settings.opacity);
            sheet.properties.SetTexture(mask, settings.mask);

            context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
        }
    }
}
