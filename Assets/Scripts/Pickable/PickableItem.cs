﻿using FPSTestProject.Characters;
using UnityEngine;

namespace FPSTestProject.Pickable
{
    public abstract class PickableItem : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            var character = other.GetComponentInParent<CharacterHolder>();
            if (character != null)
            {
                PickUp(character);
                Destroy(gameObject);
            }
        }

        protected abstract void PickUp(CharacterHolder character);
    }
}
