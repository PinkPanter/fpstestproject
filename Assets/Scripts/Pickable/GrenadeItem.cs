﻿using FPSTestProject.Characters;
using FPSTestProject.Characters.Player;
using UnityEngine;

namespace FPSTestProject.Pickable
{
    public class GrenadeItem : PickableItem
    {
        #region Serializable

        [SerializeField]
        [Range(0, 10)]
        public int count;

        #endregion

        protected override void PickUp(CharacterHolder character)
        {
           character.GetCharacterComponent<PlayerWeaponController>()?.AddGrenade(count);
        }
    }
}
