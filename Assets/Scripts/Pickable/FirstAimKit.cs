﻿using FPSTestProject.Characters;
using FPSTestProject.Helpers.Runtime.SoundManager;
using UnityEngine;

namespace FPSTestProject.Pickable
{
    public class FirstAimKit : PickableItem
    {
        #region Serializable

        [SerializeField]
        [Range(0, 200)]
        public int healingEffect;

        #endregion

        protected override void PickUp(CharacterHolder character)
        {
            character.Heal(healingEffect);
            SoundManager.Instance.PlaySFX(SoundManagerDatabase.GetRandomClip(SoundType.Heal), transform.position,
                0.5f);
        }
    }
}
