﻿using FPSTestProject.Helpers.Runtime.SoundManager;
using FPSTestProject.Managers;
using FPSTestProject.UI.Layout;
using UnityEngine;

namespace FPSTestProject.UI
{
    public class PauseLayout : LayoutPage
    {
        public override void Open()
        {
            base.Open();

            Time.timeScale = 0;

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                LayoutManager.OpenLayout("Gameplay");
        }

        public void BackToGame()
        {
            SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);

            LayoutManager.OpenLayout("Gameplay");
        }


        public void OpenSettings()
        {
            SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);

            LayoutManager.OpenLayout("Settings");
        }

        public void BackToMenu()
        {
            SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);

            Time.timeScale = 1;
            LoadingManager.LoadScene(1);
        }
    }
}
