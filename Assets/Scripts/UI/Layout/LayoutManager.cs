﻿using System.Collections.Generic;
using UnityEngine;

namespace FPSTestProject.UI.Layout
{
    public class LayoutManager : MonoBehaviour
    {
        private static LayoutManager layoutManager;

        private Dictionary<string, LayoutPage> elements = new Dictionary<string, LayoutPage>();

        private string current;

        private void Awake()
        {
            layoutManager = this;

            var children = GetComponentsInChildren<LayoutPage>(true);
            foreach (LayoutPage element in children)
            {
                if (element.gameObject.activeSelf)
                    current = element.Key;

                if (elements.ContainsKey(element.Key))
                {
                    if (elements[element.Key] == null)
                        elements[element.Key] = element;
                }
                else
                    elements.Add(element.Key, element);

            }
        }

        public static void OpenLayout(string key)
        {
            if (layoutManager == null)
            {
                Debug.LogWarning("LayoutManager is null");
            }

            layoutManager.OpenLayoutInternal(key);
        }

        private void OpenLayoutInternal(string key)
        {
            if (string.IsNullOrEmpty(key))
                return;

            if (!string.IsNullOrEmpty(current))
                elements[current].Close();

            if (elements.ContainsKey(key))
            {
                current = key;
                elements[key].Open();
            }
        }
    }
}
