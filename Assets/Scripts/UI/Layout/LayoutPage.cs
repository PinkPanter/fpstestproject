﻿using UnityEngine;

namespace FPSTestProject.UI.Layout
{
    public abstract class LayoutPage : MonoBehaviour
    {
        #region Serializable

        [SerializeField]
        private string key;
        
        #endregion

        public string Key => key;

        protected LayoutManager layoutManager;

        protected virtual void Awake()
        {
            layoutManager = GetComponentInParent<LayoutManager>();
        }

        public virtual void Open()
        {
            gameObject.SetActive(true);
        }

        public virtual void Close()
        {
            gameObject.SetActive(false);
        }
    }
}
