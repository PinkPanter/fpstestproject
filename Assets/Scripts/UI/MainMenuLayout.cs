﻿using FPSTestProject.Helpers.Runtime.SoundManager;
using FPSTestProject.Managers;
using FPSTestProject.UI.Layout;
using UnityEngine;

namespace FPSTestProject.UI
{
    public class MainMenuLayout : LayoutPage
    {
        #region Serializable

        [SerializeField]
        private AudioClip mainMenuMusic;

        #endregion

        protected override void Awake()
        {
            SoundManager.Instance.PlayMusic(mainMenuMusic, 0.5f);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        public void Tutorial()
        {
            SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            SoundManager.Instance.StopAllMusic();
            LoadingManager.LoadScene(3);
        }

        public void NewGame()
        {
            SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            SoundManager.Instance.StopAllMusic();
            LoadingManager.LoadScene(2);
        }

        public void OpenSettings()
        {
            SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);

            LayoutManager.OpenLayout("Settings");
        }

        public void Exit()
        {
            SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);

            Application.Quit();
        }
    }
}
