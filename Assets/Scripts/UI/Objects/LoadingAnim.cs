﻿using System.Collections.Generic;
using UnityEngine;

namespace FPSTestProject.UI.Objects
{
    public class LoadingAnim : MonoBehaviour
    {
        public Dictionary<Transform, Vector3> originalPos = new Dictionary<Transform, Vector3>();

        [Range(1, 20f)]
        public float speed = 1f;
    
        public AnimationCurve curve;

        private float currentState;

        private bool increase = true;

        private void Start()
        {
            foreach (Transform child in transform)
            {
                originalPos.Add(child, child.localPosition);
            }
        }

        private void Update()
        {
            currentState += Time.deltaTime * speed * (increase ? 1 : -1);
            if (currentState > 1)
                increase = false;
            else if(currentState < 0)
                increase = true;

            var currentStateByCurve = curve.Evaluate(currentState);
            foreach (var child in originalPos)
            {
                child.Key.localPosition = currentStateByCurve * child.Value;
            }
        }
    }
}
