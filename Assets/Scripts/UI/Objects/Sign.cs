﻿using UnityEngine;
using UnityEngine.UI;

namespace FPSTestProject.UI.Objects
{
    public class Sign : MonoBehaviour
    {
        private static Sign instance;

        public static Sign Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<Sign>();
                    if (instance == null)
                    {
                        var canvases = FindObjectsOfType<Canvas>();
                        foreach (Canvas canvas in canvases)
                        {
                            if (canvas.renderMode == RenderMode.ScreenSpaceOverlay)
                            {
                                instance = Instantiate(Resources.Load<GameObject>("Prefabs/UI/Sign"), canvas.transform)
                                    .GetComponent<Sign>();
                            }
                        }
                    }
                }

                return instance;
            }
        }

        #region Serializable

        [SerializeField]
        private Image image;

        #endregion

        public Sprite SignIcon
        {
            get => image.sprite;
            set => image.sprite = value;
        }

        public bool IsVisible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }

        private void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
            }
        }
    }
}
