﻿using System.Collections;
using DG.Tweening;
using FPSTestProject.Characters;
using FPSTestProject.Characters.Player;
using FPSTestProject.UI.Layout;
using FPSTestProject.UI.Objects;
using FPSTestProject.Weapons;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace FPSTestProject.UI
{
    public class GameplayLayout : LayoutPage
    {
        #region Serializable

        [SerializeField]
        private Slider healthSlider;
        [SerializeField]
        private TextMeshProUGUI healthCount;

        [SerializeField]
        private Image weaponIcon;
        [SerializeField]
        private TextMeshProUGUI weaponBulletsCounter;
        [SerializeField]
        private TextMeshProUGUI grenadesCount;

        [SerializeField]
        private TextMeshProUGUI deadSign;

        #endregion

        private void Start()
        {
            if (CharacterHolder.LocalPlayer != null)
            {
                CharacterHolder.LocalPlayer.OnHealthChanged += LocalPlayerOnOnHealthChanged;
                LocalPlayerOnOnHealthChanged();

                var weaponController = CharacterHolder.LocalPlayer.GetCharacterComponent<PlayerWeaponController>();

                weaponController.OnGrenadesChanged += OnGrenadesChanged;
                OnGrenadesChanged(weaponController.GrenatesCount);

                weaponController.OnWeaponChanged += OnWeaponChanged;
                OnWeaponChanged(weaponController.CurrentWeapon);

                weaponController.OnWeaponBulletsChanged += OnWeaponBulletsChanged;
                OnWeaponBulletsChanged(weaponController.CurrentWeapon);

                CharacterHolder.LocalPlayer.OnDead += LocalPlayerOnOnDead;
            }

        }

        public override void Open()
        {
            base.Open();

            Time.timeScale = 1;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void LocalPlayerOnOnHealthChanged()
        {
            healthSlider.value = 1.0f * CharacterHolder.LocalPlayer.CurrentHealth / CharacterHolder.LocalPlayer.MaxHealth;
            healthCount.text = Mathf.Clamp(CharacterHolder.LocalPlayer.CurrentHealth, 0, 100000).ToString();
        }

        private void OnGrenadesChanged(int count)
        {
            grenadesCount.text = count.ToString();
        }
        
        private void OnWeaponBulletsChanged(Weapon weapon)
        {
            weaponBulletsCounter.text = $"{weapon.CurrentClipCount}/{weapon.TotalBullets}";
        }

        private void OnWeaponChanged(Weapon weapon)
        {
            weaponIcon.sprite = weapon.Icon;
            OnWeaponBulletsChanged(weapon);
        }

        private void LocalPlayerOnOnDead()
        {
            Sign.Instance.IsVisible = false;
            deadSign.gameObject.SetActive(true);
            StartCoroutine(ShowDeadSign());
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                LayoutManager.OpenLayout("Pause");
        }

        private IEnumerator ShowDeadSign()
        {
            const float signAnimTime = 2;
            float startTime = Time.time;
            var fromColor = deadSign.color;
            fromColor.a = 0;
            deadSign.color = fromColor;

            while (Time.time - startTime < signAnimTime)
            {
                fromColor.a = (Time.time - startTime) / signAnimTime;
                deadSign.color = fromColor;
                yield return null;
            }

            fromColor.a = 1;
            deadSign.color = fromColor;
        }

        private void OnDestroy()
        {
            if (CharacterHolder.LocalPlayer != null)
            {
                CharacterHolder.LocalPlayer.OnHealthChanged -= LocalPlayerOnOnHealthChanged;
            }
        }
    }
}
