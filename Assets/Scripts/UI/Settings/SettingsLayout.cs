﻿using System;
using FPSTestProject.Helpers.Runtime.Configs;
using FPSTestProject.Helpers.Runtime.SoundManager;
using FPSTestProject.UI.Layout;
using FPSTestProject.UI.Settings;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace FPSTestProject.UI
{
    public class SettingsLayout : LayoutPage
    {
        #region Serializable

        [SerializeField]
        private string returnTo = "MainMenu";

        [SerializeField]
        private Panel[] panels;

        #region SoundSettings

        [Space]
        [SerializeField]
        private Slider generalSlider;
        [SerializeField]
        private Slider musicSlider;
        [SerializeField]
        private Slider sfxSlider;
        [SerializeField]
        private Slider uiSlider;

        [Space]
        [SerializeField]
        private Transform content;
        [SerializeField]
        private SettingsElement element;

        #endregion

        #endregion

        private Panel currentPanel;

        private void Start()
        {
            foreach (Panel panel in panels)
            {
                panel.button.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
                panel.panelObj.SetActive(false);
                panel.button.onClick.AddListener(() => OpenPanel(panel));
            }

            OpenPanel(panels[0]);

            InitializeSound();
            InitializeConfigs();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Back();
        }

        public void Back()
        {
            SoundManager.Instance.SaveSettings();
            LayoutManager.OpenLayout(returnTo);
        }

        private void OpenPanel(Panel panel)
        {
            if(panel.button == currentPanel.button)
                return;

            if (currentPanel.panelObj != null)
            {
                currentPanel.button.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
                currentPanel.panelObj.SetActive(false);
                SoundManager.Instance.PlayUI(SoundManagerDatabase.GetRandomClip(SoundType.UiClick), 0.5f);
            }

            panel.button.transform.localScale = Vector3.one;
            panel.panelObj.SetActive(true);

            currentPanel = panel;
        }

        private void InitializeSound()
        {
            generalSlider.value = SoundManager.Instance.GlobalVolume;
            musicSlider.value = SoundManager.Instance.MusicVolume;
            sfxSlider.value = SoundManager.Instance.SfxVolume;
            uiSlider.value = SoundManager.Instance.UiVolume;

            generalSlider.onValueChanged.AddListener(volume => SoundManager.Instance.GlobalVolume = volume);
            musicSlider.onValueChanged.AddListener(volume => SoundManager.Instance.MusicVolume = volume);
            sfxSlider.onValueChanged.AddListener(volume => SoundManager.Instance.SfxVolume = volume);
            uiSlider.onValueChanged.AddListener(volume => SoundManager.Instance.UiVolume = volume);
        }

        private void InitializeConfigs()
        {
            var allFields = ConfigController.GetAllConfigurableFields();

            foreach (var fields in allFields)
            {
                foreach (var field in fields.Value)
                {
                    Instantiate(element.gameObject, content.transform).GetComponent<SettingsElement>()
                        .Init(fields.Key, field);
                }
            }
        }

        [Serializable]
        private struct Panel
        {
            public Button button;

            [FormerlySerializedAs("panel")] public GameObject panelObj;
        }
    }
}
