﻿using System;
using System.Globalization;
using System.Reflection;
using FPSTestProject.Helpers.Runtime.Configs;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace FPSTestProject.UI.Settings
{
    public class SettingsElement : MonoBehaviour
    {
        #region Serialized

        [SerializeField]
        private TextMeshProUGUI propName;

        [SerializeField]
        private TMP_InputField inputField;

        [SerializeField]
        private Image saveImg;

        #endregion

        private Type type;
        private FieldInfo field;

        public void Init(Type type, FieldInfo field)
        {
            this.type = type;
            this.field = field;

            propName.text = $"{type.Name}.{field.Name}({field.GetCustomAttribute<ConfigurableAttribute>().displayName})";

            if (field.FieldType == typeof(int))
            {
                inputField.characterValidation = TMP_InputField.CharacterValidation.Integer;
                inputField.text = ConfigController.GetInt($"{type.Name}.{field.Name}")?.ToString();
            }
            else if (field.FieldType == typeof(float))
            {
                inputField.characterValidation = TMP_InputField.CharacterValidation.Decimal;
                inputField.text = ConfigController.GetFloat($"{type.Name}.{field.Name}")?.ToString("F3");
            }
            else
            {
                inputField.characterValidation = TMP_InputField.CharacterValidation.None;
                inputField.text = ConfigController.GetString($"{type.Name}.{field.Name}");
            }
        }

        public void Save()
        {
            if (field.FieldType == typeof(int))
            {
                if (int.TryParse(inputField.text, out var outInt))
                {
                    saveImg.color = Color.green;
                    ConfigController.Save($"{type.Name}.{field.Name}", inputField.text);
                }
                else
                    saveImg.color = Color.red;
            }
            else if (field.FieldType == typeof(float))
            {
                if (float.TryParse(inputField.text.Replace(",","."), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var outFloat))
                {
                    saveImg.color = Color.green;
                    ConfigController.Save($"{type.Name}.{field.Name}",
                        outFloat.ToString("F3", CultureInfo.InvariantCulture));
                }
                else
                    saveImg.color = Color.red;
            }
            else
            {
                ConfigController.Save($"{type.Name}.{field.Name}", inputField.text);
                saveImg.color = Color.green;
            }
        }
    }
}
