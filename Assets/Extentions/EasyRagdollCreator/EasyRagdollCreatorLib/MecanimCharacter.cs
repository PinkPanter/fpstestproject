﻿using UnityEngine;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000003 RID: 3
	public class MecanimCharacter : MonoBehaviour
	{
		// Token: 0x06000004 RID: 4 RVA: 0x000020F0 File Offset: 0x000002F0
		private void Awake()
		{
			this.actualAnimator = base.GetComponent<Animator>();
			if (!this.actualAnimator)
			{
				Debug.Log("No animator inside " + base.gameObject.name + ", disabling component");
				base.enabled = false;
				return;
			}
			if (this.life > 0f)
			{
				this.SwitchOn(this.life);
				return;
			}
			this.SwitchOff();
		}

		// Token: 0x06000005 RID: 5 RVA: 0x0000215D File Offset: 0x0000035D
		public void SwitchOn(float _life)
		{
			this.life = _life;
			this.actualAnimator.enabled = true;
			Functions.DeactivateAllRigidBodiesIncludingChilds(base.transform);
		}

		// Token: 0x06000006 RID: 6 RVA: 0x0000217D File Offset: 0x0000037D
		public void SwitchOff()
		{
			this.life = 0f;
			this.actualAnimator.enabled = false;
			Functions.ActivateAllRigidBodiesIncludingChilds(base.transform);
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000021A1 File Offset: 0x000003A1
		private void Update()
		{
			if (Input.GetKey(this.keyToSwitchOn))
			{
				this.SwitchOn(100f);
			}
			if (Input.GetKey(this.keyToSwitchOff))
			{
				this.SwitchOff();
			}
		}

		// Token: 0x04000001 RID: 1
		public float life = 100f;

		// Token: 0x04000002 RID: 2
		public KeyCode keyToSwitchOff = KeyCode.D;

		// Token: 0x04000003 RID: 3
		public KeyCode keyToSwitchOn = KeyCode.R;

		// Token: 0x04000004 RID: 4
		private Animator actualAnimator;
	}
}
