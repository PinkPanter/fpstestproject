﻿using UnityEngine;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000009 RID: 9
	public class RagdollCharacter : MonoBehaviour
    {
        [SerializeField]
        private Collider[] ragdollCollider;
        [SerializeField]
        private Rigidbody[] ragdollRigidbodies;
       // [SerializeField]
       // private Joint[] ragdollJoint;

        public void SetUpRagdoll(Collider[] colliders, Rigidbody[] rigidbodies, Joint[] joints)
        {
            ragdollCollider = colliders;
            ragdollRigidbodies = rigidbodies;
           // ragdollJoint = joints;
        }

        public void ActiveRagdoll(bool active)
        {
            foreach (var rigidbody in ragdollRigidbodies)
            {
                rigidbody.isKinematic = !active;
            }
            foreach (var collider in ragdollCollider)
            {
                collider.enabled = active;
            }

           //foreach (var joint in ragdollJoint)
           //{
           //    joint. = active;
           //}
        }
    }
}
