﻿using System;
using UnityEngine;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000007 RID: 7
	public class PhysicsFixer : MonoBehaviour
	{
		// Token: 0x06000015 RID: 21 RVA: 0x00002553 File Offset: 0x00000753
		private void Start()
		{
			this.ModifyAllJointsRecursively(base.transform);
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002564 File Offset: 0x00000764
		private void ModifyAllJointsRecursively(Transform src)
		{
			if (src.GetComponent<CharacterJoint>())
			{
				src.GetComponent<CharacterJoint>().enableProjection = true;
			}
			if (src.GetComponent<Rigidbody>())
			{
				src.GetComponent<Rigidbody>().mass = this.massMultiplier * src.GetComponent<Rigidbody>().mass;
			}
			foreach (object obj in src)
			{
				Transform src2 = (Transform)obj;
				this.ModifyAllJointsRecursively(src2);
			}
		}

		// Token: 0x0400001A RID: 26
		private float massMultiplier = 6f;
	}
}
