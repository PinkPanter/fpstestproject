﻿using System;
using System.Collections;
using UnityEngine;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000006 RID: 6
	public class OwnBoneInfo
	{
		// Token: 0x0400000C RID: 12
		public string name;

		// Token: 0x0400000D RID: 13
		public Transform anchor;

		// Token: 0x0400000E RID: 14
		public CharacterJoint joint;

		// Token: 0x0400000F RID: 15
		public OwnBoneInfo parent;

		// Token: 0x04000010 RID: 16
		public float minLimit;

		// Token: 0x04000011 RID: 17
		public float maxLimit;

		// Token: 0x04000012 RID: 18
		public float swingLimit;

		// Token: 0x04000013 RID: 19
		public Vector3 axis;

		// Token: 0x04000014 RID: 20
		public Vector3 normalAxis;

		// Token: 0x04000015 RID: 21
		public float radiusScale;

		// Token: 0x04000016 RID: 22
		public Type colliderType;

		// Token: 0x04000017 RID: 23
		public ArrayList children = new ArrayList();

		// Token: 0x04000018 RID: 24
		public float density;

		// Token: 0x04000019 RID: 25
		public float summedMass;
	}
}
