﻿using System;
using UnityEngine;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000002 RID: 2
	public class Functions
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static Transform GetParentRagdollCharacter(Transform current)
		{
			Transform transform = current.transform;
			while (transform != null)
			{
				if (transform.GetComponent<RagdollCharacter>())
				{
					return transform;
				}
				if (transform.parent)
				{
					transform = transform.parent.transform;
				}
				else
				{
					transform = null;
				}
			}
			return null;
		}

		// Token: 0x06000002 RID: 2 RVA: 0x0000209C File Offset: 0x0000029C
		public static Transform GetParentMecanimCharacter(Transform current)
		{
			Transform transform = current.transform;
			while (transform != null)
			{
				if (transform.GetComponent<MecanimCharacter>())
				{
					return transform;
				}
				if (transform.parent)
				{
					transform = transform.parent.transform;
				}
				else
				{
					transform = null;
				}
			}
			return null;
		}

        public static void DeactivateAllRigidBodiesIncludingChilds(Transform transform)
        {
            var allRigidbodies = transform.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody rigidbody in allRigidbodies)
            {
                rigidbody.isKinematic = true;
            }
        }

        public static void ActivateAllRigidBodiesIncludingChilds(Transform transform)
        {
            var allRigidbodies = transform.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody rigidbody in allRigidbodies)
            {
                rigidbody.isKinematic = false;
            }
        }
    }
}
