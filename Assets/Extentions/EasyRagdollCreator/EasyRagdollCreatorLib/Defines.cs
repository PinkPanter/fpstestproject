﻿using System;
using UnityEngine;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000004 RID: 4
	public class Defines
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000009 RID: 9 RVA: 0x000021F1 File Offset: 0x000003F1
		public static string CHARACTER_PATH_PREFIX
		{
			get
			{
				return "CharacterPath_";
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000A RID: 10 RVA: 0x000021F8 File Offset: 0x000003F8
		public static string CHARACTER_GROUP_PREFIX
		{
			get
			{
				return "CharacterGroup_";
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000B RID: 11 RVA: 0x000021FF File Offset: 0x000003FF
		public static string GENERAL_CHARACTER_PATH_NODE_PREFIX
		{
			get
			{
				return "Node_";
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000C RID: 12 RVA: 0x00002206 File Offset: 0x00000406
		public static string CHARACTERS_CONTAINER_NAME
		{
			get
			{
				return "Characters";
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000D RID: 13 RVA: 0x0000220D File Offset: 0x0000040D
		public static string CHARACTERS_HANDLER_NAME
		{
			get
			{
				return "CharacterHandler";
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000E RID: 14 RVA: 0x00002214 File Offset: 0x00000414
		public static string LABELS_TEXT_COLOR
		{
			get
			{
				return "#2f2f2f";
			}
		}

		// Token: 0x04000005 RID: 5
		public static Vector3 NOT_VISIBLE = -99999f * Vector3.one;
	}
}
