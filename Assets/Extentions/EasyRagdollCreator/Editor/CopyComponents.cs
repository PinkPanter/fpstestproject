﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000002 RID: 2
	public class CopyComponents : MonoBehaviour
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private static void RemoveComponentInsideGameObject(GameObject gameObject, Type t)
		{
			Component[] components = gameObject.GetComponents<Component>();
			SerializedObject serializedObject = new SerializedObject(gameObject);
			SerializedProperty serializedProperty = serializedObject.FindProperty("m_Component");
			int num = 0;
			for (int i = 0; i < components.Length; i++)
			{
				if (components[i].GetType() == t)
				{
					serializedProperty.DeleteArrayElementAtIndex(i - num);
					num++;
				}
			}
			serializedObject.ApplyModifiedProperties();
		}

		// Token: 0x06000002 RID: 2 RVA: 0x000020AC File Offset: 0x000002AC
		public static GameObject FindChildWithName(GameObject go, string name)
		{
			foreach (object obj in go.transform)
			{
				GameObject gameObject = ((Transform)obj).gameObject;
				if (gameObject.name == name)
				{
					return gameObject;
				}
			}
			return null;
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002118 File Offset: 0x00000318
		public static void RecursiveRagdollComponentsCopy(GameObject src, GameObject dst, GameObject baseDestinationObject)
		{
			foreach (Component component in src.GetComponents(typeof(Component)))
			{
				if (component is Rigidbody || component is Joint || component is Collider)
				{
					if (dst.GetComponent(component.GetType()))
					{
						CopyComponents.RemoveComponentInsideGameObject(dst, component.GetType());
					}
					Object @object = dst.AddComponent(component.GetType());
					SerializedObject serializedObject = new SerializedObject(component);
					SerializedObject serializedObject2 = new SerializedObject(@object);
					serializedObject.Update();
					serializedObject2.Update();
					SerializedProperty iterator = serializedObject.GetIterator();
					bool flag = true;
					while (iterator.Next(flag))
					{
						flag = true;
						string propertyPath = iterator.propertyPath;
						bool flag2 = false;
						foreach (string value in CopyComponents.propertyBlacklist)
						{
							if (propertyPath.EndsWith(value))
							{
								flag2 = true;
								break;
							}
						}
						if (flag2)
						{
							flag = false;
						}
						else
						{
							SerializedProperty to = serializedObject2.FindProperty(propertyPath);
							CopyComponents.AssignSerializedProperty(iterator, to);
						}
					}
					serializedObject2.ApplyModifiedProperties();
				}
			}
			Joint component2 = src.GetComponent<Joint>();
			if (component2)
			{
				string name = component2.connectedBody.gameObject.name;
				Transform transformInChildsByName = CopyComponents.GetTransformInChildsByName(baseDestinationObject.transform, name);
				if (transformInChildsByName.GetComponent<Rigidbody>())
				{
					dst.GetComponent<Joint>().connectedBody = transformInChildsByName.GetComponent<Rigidbody>();
				}
				else
				{
					Debug.Log("No rigidbody found for the joint inside " + dst.gameObject.name);
				}
			}
			foreach (object obj in src.transform)
			{
				GameObject gameObject = ((Transform)obj).gameObject;
				string name2 = gameObject.name;
				GameObject gameObject2 = CopyComponents.FindChildWithName(dst, name2);
				if (gameObject2 == null)
				{
					Debug.LogWarning("Didn't find matching GameObject for " + gameObject.name);
				}
				else
				{
					CopyComponents.RecursiveRagdollComponentsCopy(gameObject, gameObject2, baseDestinationObject);
				}
			}
		}

		// Token: 0x06000004 RID: 4 RVA: 0x0000232C File Offset: 0x0000052C
		public static Transform GetTransformInChildsByName(Transform mainTransform, string name)
		{
			if (mainTransform.name == name)
			{
				return mainTransform;
			}
			foreach (object obj in mainTransform)
			{
				Transform transformInChildsByName = CopyComponents.GetTransformInChildsByName((Transform)obj, name);
				if (transformInChildsByName != null)
				{
					return transformInChildsByName;
				}
			}
			return null;
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000023A0 File Offset: 0x000005A0
		public static void ComponentsCopy(GameObject src, GameObject dst)
		{
			foreach (Component component in src.GetComponents(typeof(Component)))
			{
				if (component is Rigidbody || component is CharacterJoint || component is SphereCollider || component is BoxCollider || component is CapsuleCollider)
				{
					Object @object = dst.AddComponent(component.GetType());
					SerializedObject serializedObject = new SerializedObject(component);
					SerializedObject serializedObject2 = new SerializedObject(@object);
					serializedObject.Update();
					serializedObject2.Update();
					SerializedProperty iterator = serializedObject.GetIterator();
					bool flag = true;
					while (iterator.Next(flag))
					{
						flag = true;
						string propertyPath = iterator.propertyPath;
						bool flag2 = false;
						foreach (string value in CopyComponents.propertyBlacklist)
						{
							if (propertyPath.EndsWith(value))
							{
								flag2 = true;
								break;
							}
						}
						if (flag2)
						{
							flag = false;
						}
						else
						{
							SerializedProperty to = serializedObject2.FindProperty(propertyPath);
							CopyComponents.AssignSerializedProperty(iterator, to);
						}
					}
					serializedObject2.ApplyModifiedProperties();
				}
			}
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000024A8 File Offset: 0x000006A8
		public static void RecursiveComponentsCopy(GameObject src, GameObject dst, bool scriptsAllowed, bool rigidBodiesAllowed, bool meshColliderAllowed)
		{
			foreach (Component component in src.GetComponents(typeof(Component)))
			{
				if (!(component is Transform) && !(component is Renderer) && !(component is Animation) && !(component is MeshFilter) && (!(component is Rigidbody) || rigidBodiesAllowed) && (!(component is MeshCollider) || meshColliderAllowed))
				{
					if (dst.GetComponent(component.GetType()))
					{
						CopyComponents.RemoveComponentInsideGameObject(dst, component.GetType());
					}
					Object @object = dst.AddComponent(component.GetType());
					SerializedObject serializedObject = new SerializedObject(component);
					SerializedObject serializedObject2 = new SerializedObject(@object);
					serializedObject.Update();
					serializedObject2.Update();
					SerializedProperty iterator = serializedObject.GetIterator();
					bool flag = true;
					while (iterator.Next(flag))
					{
						flag = true;
						string propertyPath = iterator.propertyPath;
						bool flag2 = false;
						foreach (string value in CopyComponents.propertyBlacklist)
						{
							if (propertyPath.EndsWith(value))
							{
								flag2 = true;
								break;
							}
						}
						if (flag2)
						{
							flag = false;
						}
						else
						{
							SerializedProperty to = serializedObject2.FindProperty(propertyPath);
							CopyComponents.AssignSerializedProperty(iterator, to);
						}
					}
					serializedObject2.ApplyModifiedProperties();
				}
			}
			foreach (object obj in src.transform)
			{
				GameObject gameObject = ((Transform)obj).gameObject;
				string name = gameObject.name;
				GameObject gameObject2 = CopyComponents.FindChildWithName(dst, name);
				if (gameObject2 == null)
				{
					Debug.LogWarning("Didn't find matching GameObject for " + gameObject.name);
				}
				else
				{
					CopyComponents.RecursiveComponentsCopy(gameObject, gameObject2, scriptsAllowed, rigidBodiesAllowed, meshColliderAllowed);
				}
			}
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002688 File Offset: 0x00000888
		private static void AssignSerializedProperty(SerializedProperty from, SerializedProperty to)
		{
			if (from.propertyType != to.propertyType)
			{
				Debug.LogError("SerializedPropertys have different types!");
			}
			switch (from.propertyType)
			{
			case SerializedPropertyType.Integer:
            case SerializedPropertyType.LayerMask:
                    to.intValue = from.intValue;
				return;
			case SerializedPropertyType.Boolean:
				to.boolValue = from.boolValue;
				return;
			case SerializedPropertyType.Float:
				to.floatValue = from.floatValue;
				return;
			case SerializedPropertyType.String:
				to.stringValue = from.stringValue;
				return;
			case SerializedPropertyType.Color:
				to.colorValue = from.colorValue;
				return;
			case SerializedPropertyType.ObjectReference:
				to.objectReferenceValue = from.objectReferenceValue;
				return;
			case SerializedPropertyType.Enum:
				to.enumValueIndex = from.enumValueIndex;
				return;
			case SerializedPropertyType.Vector2:
				to.vector2Value = from.vector2Value;
				return;
			case SerializedPropertyType.Vector3:
				to.vector3Value = from.vector3Value;
				return;
			case SerializedPropertyType.Rect:
				to.rectValue = from.rectValue;
				return;
			case SerializedPropertyType.ArraySize:
				to.intValue = from.intValue;
				return;
			case SerializedPropertyType.Character:
				to.intValue = from.intValue;
				return;
			case SerializedPropertyType.AnimationCurve:
				to.animationCurveValue = from.animationCurveValue;
				return;
			case SerializedPropertyType.Bounds:
				to.boundsValue = from.boundsValue;
				break;
			default:
				return;
			}
		}

		// Token: 0x04000001 RID: 1
		public GameObject source;

		// Token: 0x04000002 RID: 2
		public GameObject destination;

		// Token: 0x04000003 RID: 3
		private static string[] propertyBlacklist = new string[]
		{
			"m_ObjectHideFlags",
			"m_PrefabParentObject",
			"m_PrefabInternal",
			"m_GameObject",
			"m_EditorHideFlags",
			"m_FileID",
			"m_PathID"
		};
	}
}
