﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000003 RID: 3
	public class EditorFunctions : MonoBehaviour
	{
		// Token: 0x0600000B RID: 11 RVA: 0x000028EB File Offset: 0x00000AEB
		public static bool GetActualSelectionIsRagdollCharacter()
		{
			return Selection.activeGameObject && Functions.GetParentRagdollCharacter(Selection.activeGameObject.transform);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002914 File Offset: 0x00000B14
		public static Transform FindComponentInChildByStringChain(Transform current, string name)
		{
			if (current.name.ToLower().Contains(name.ToLower()))
			{
				return current;
			}
			for (int i = 0; i < current.childCount; i++)
			{
				Transform transform = EditorFunctions.FindComponentInChildByStringChain(current.GetChild(i), name);
				if (transform != null)
				{
					return transform;
				}
			}
			return null;
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002968 File Offset: 0x00000B68
		public static void CleanScripts(Transform current)
		{
			RagdollCharacter component3 = current.GetComponent<RagdollCharacter>();
			MecanimCharacter component4 = current.GetComponent<MecanimCharacter>();
			if (component3)
			{
				Object.DestroyImmediate(component3);
			}
			if (component4)
			{
				Object.DestroyImmediate(component4);
			}
			for (int i = 0; i < current.childCount; i++)
			{
				EditorFunctions.CleanScripts(current.GetChild(i));
			}
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000029EC File Offset: 0x00000BEC
		public static void SetShowHelpersRecursively(Transform current, bool show)
		{
			for (int i = 0; i < current.childCount; i++)
			{
				EditorFunctions.SetShowHelpersRecursively(current.GetChild(i), show);
			}
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002A30 File Offset: 0x00000C30
		public static void SetShowRenderersRecursively(Transform current, bool show)
		{
			Renderer component = current.GetComponent<Renderer>();
			if (component)
			{
				component.enabled = show;
			}
			for (int i = 0; i < current.childCount; i++)
			{
				EditorFunctions.SetShowRenderersRecursively(current.GetChild(i), show);
			}
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002A74 File Offset: 0x00000C74
		public static Mesh GenerateCapsuleMesh(float height, float radius, int segments)
		{
			if (segments % 2 != 0)
			{
				segments++;
			}
			int num = segments + 1;
			float[] array = new float[num];
			float[] array2 = new float[num];
			float[] array3 = new float[num];
			float[] array4 = new float[num];
			float num2 = 0f;
			float num3 = 0f;
			for (int i = 0; i < num; i++)
			{
				array[i] = Mathf.Sin(num2 * 0.0174532924f);
				array2[i] = Mathf.Cos(num2 * 0.0174532924f);
				array3[i] = Mathf.Cos(num3 * 0.0174532924f);
				array4[i] = Mathf.Sin(num3 * 0.0174532924f);
				num2 += 360f / (float)segments;
				num3 += 180f / (float)segments;
			}
			Vector3[] array5 = new Vector3[num * (num + 1)];
			Vector2[] array6 = new Vector2[array5.Length];
			int num4 = 0;
			float num5 = (height - radius * 2f) * 0.5f;
			if (num5 < 0f)
			{
				num5 = 0f;
			}
			float num6 = 1f / (float)(num - 1);
			int num7 = Mathf.CeilToInt((float)num * 0.5f);
			for (int j = 0; j < num7; j++)
			{
				for (int k = 0; k < num; k++)
				{
					array5[num4] = new Vector3(array[k] * array4[j], array3[j], array2[k] * array4[j]) * radius;
					array5[num4].y = num5 + array5[num4].y;
					float num8 = 1f - num6 * (float)k;
					float num9 = (array5[num4].y + height * 0.5f) / height;
					array6[num4] = new Vector2(num8, num9);
					num4++;
				}
			}
			for (int l = Mathf.FloorToInt((float)num * 0.5f); l < num; l++)
			{
				for (int m = 0; m < num; m++)
				{
					array5[num4] = new Vector3(array[m] * array4[l], array3[l], array2[m] * array4[l]) * radius;
					array5[num4].y = -num5 + array5[num4].y;
					float num8 = 1f - num6 * (float)m;
					float num9 = (array5[num4].y + height * 0.5f) / height;
					array6[num4] = new Vector2(num8, num9);
					num4++;
				}
			}
			int[] array7 = new int[segments * (segments + 1) * 2 * 3];
			int n = 0;
			int num10 = 0;
			while (n < segments + 1)
			{
				int num11 = 0;
				while (num11 < segments)
				{
					array7[num10] = n * (segments + 1) + num11;
					array7[num10 + 1] = (n + 1) * (segments + 1) + num11;
					array7[num10 + 2] = (n + 1) * (segments + 1) + num11 + 1;
					array7[num10 + 3] = n * (segments + 1) + num11 + 1;
					array7[num10 + 4] = n * (segments + 1) + num11;
					array7[num10 + 5] = (n + 1) * (segments + 1) + num11 + 1;
					num11++;
					num10 += 6;
				}
				n++;
			}
			Mesh mesh = new Mesh();
			mesh.Clear();
			mesh.name = "ProceduralCapsule";
			mesh.vertices = array5;
			mesh.uv = array6;
			mesh.triangles = array7;
			mesh.RecalculateBounds();
			mesh.RecalculateNormals();
			return mesh;
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002DE5 File Offset: 0x00000FE5
		public static void DrawLineSeparator()
		{
			GUILayout.Box("", new GUILayoutOption[]
			{
				GUILayout.ExpandWidth(true),
				GUILayout.Height(1f)
			});
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002E10 File Offset: 0x00001010
		public static void DrawEditorBox(string text)
		{
			Color backgroundColor = GUI.backgroundColor;
			GUI.backgroundColor = Color.red;
			GUILayout.Box(text, new GUIStyle(GUI.skin.box)
			{
				normal = 
				{
					textColor = Color.white
				},
				fontStyle = FontStyle.Bold
			}, new GUILayoutOption[]
			{
				GUILayout.ExpandWidth(true)
			});
			GUI.backgroundColor = backgroundColor;
		}
	}
}
