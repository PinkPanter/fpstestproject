﻿using System;
using UnityEditor;
using UnityEngine;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000009 RID: 9
	[ExecuteInEditMode]
	public class RagdollCharacterEditor : MonoBehaviour
	{
		// Token: 0x06000062 RID: 98 RVA: 0x00006DCD File Offset: 0x00004FCD
		private void Start()
		{
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00006DCD File Offset: 0x00004FCD
		private void OnDestroy()
		{
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00007270 File Offset: 0x00005470
		private void OnEnable()
		{
			EditorApplication.update = (EditorApplication.CallbackFunction)Delegate.Combine(EditorApplication.update, new EditorApplication.CallbackFunction(this.Update));
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00006DCD File Offset: 0x00004FCD
		private void Update()
		{
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00007292 File Offset: 0x00005492
		private void OnDrawGizmos()
		{
			this.DrawVisualBones(base.transform);
		}

		// Token: 0x06000067 RID: 103 RVA: 0x000072A0 File Offset: 0x000054A0
		private void DrawVisualBones(Transform t)
		{
			if (base.transform.hasChanged)
			{
				foreach (object obj in t)
				{
					Transform transform = (Transform)obj;
					float num = 0.05f;
					Vector3 vector = new Vector3(num, 0f, 0f);
					Vector3 vector2 = new Vector3(0f, num, 0f);
					Vector3 vector3 = new Vector3(0f, 0f, num);
					vector = transform.rotation * vector;
					vector2 = transform.rotation * vector2;
					vector3 = transform.rotation * vector3;
					Gizmos.DrawLine(t.position * 0.1f + transform.position * 0.9f, t.position * 0.9f + transform.position * 0.1f);
					Gizmos.DrawLine(transform.position, transform.position + vector);
					Gizmos.DrawLine(transform.position, transform.position + vector2);
					Gizmos.DrawLine(transform.position, transform.position + vector3);
					this.DrawVisualBones(transform);
				}
			}
		}
	}
}
