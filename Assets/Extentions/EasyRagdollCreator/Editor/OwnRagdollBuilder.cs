﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BreakingTheCode.RagdollCreator
{
	// Token: 0x02000004 RID: 4
	[Serializable]
	public class OwnRagdollBuilder : EditorWindow
	{
		// Token: 0x06000014 RID: 20 RVA: 0x00002E70 File Offset: 0x00001070
		private void DrawBone(Transform bone, bool sphere, float size)
		{
			if (bone)
			{
				if (sphere)
				{
					Handles.SphereCap(0, bone.position, Quaternion.identity, size);
					return;
				}
				Handles.CubeCap(0, bone.position, Quaternion.identity, size);
			}
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002EA4 File Offset: 0x000010A4
		private void DrawBoneRelationship(Transform boneA, Transform boneB)
		{
			if (boneA && boneB)
			{
				Handles.DrawAAPolyLine(6f, new List<Vector3>
				{
					boneA.position,
					boneB.position
				}.ToArray());
			}
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002EF0 File Offset: 0x000010F0
		private void CreateCustomStyles()
		{
			this.toolbarStyle = new GUIStyle(EditorStyles.toolbarButton);
			this.toolbarStyle.fontSize = 18;
			this.toolbarStyle.fontStyle = FontStyle.Normal;
			this.toolbarStyle.border = new RectOffset(6, 6, 6, 6);
			this.toolbarStyle.normal.textColor = new Color(0f, 0f, 0f, 0.4f);
			this.toolbarStyle.active.textColor = Color.red;
			this.toolbarStyle.focused.textColor = Color.red;
			this.toolbarStyle.hover.textColor = Color.red;
			this.toolbarStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_darkGrayBG", typeof(Texture2D));
			this.toolbarStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_darkGrayBG", typeof(Texture2D));
			this.toolbarStyle.focused.background = (Texture2D)Resources.Load("EasyRagdollCreator_darkGrayBG", typeof(Texture2D));
			this.toolbarStyle.hover.background = (Texture2D)Resources.Load("EasyRagdollCreator_darkGrayBG", typeof(Texture2D));
			this.toolbarStyle.onNormal.background = (Texture2D)Resources.Load("EasyRagdollCreator_lightGrayBG", typeof(Texture2D));
			this.toolbarStyle.onActive.background = (Texture2D)Resources.Load("EasyRagdollCreator_lightGrayBG", typeof(Texture2D));
			this.toolbarStyle.onFocused.background = (Texture2D)Resources.Load("EasyRagdollCreator_darkGrayBG", typeof(Texture2D));
			this.toolbarStyle.onHover.background = (Texture2D)Resources.Load("EasyRagdollCreator_lightGrayBG", typeof(Texture2D));
			this.toolbarStyle.onNormal.textColor = Color.black;
			this.toolbarStyle.onActive.textColor = Color.red;
			this.toolbarStyle.onFocused.textColor = Color.red;
			this.toolbarStyle.onHover.textColor = Color.red;
			this.toolbarStyle.fixedHeight = 70f;
			this.toolbarStyle.padding = new RectOffset(0, 0, 0, 0);
			this.toolbarStyle.margin = new RectOffset(2, 2, 0, 0);
			this.foldOutStyle = new GUIStyle(EditorStyles.foldout);
			this.foldOutStyle.fontStyle = FontStyle.Normal;
			Color white = Color.white;
			ColorUtility.TryParseHtmlString(Defines.LABELS_TEXT_COLOR, out white);
			this.foldOutStyle.normal.textColor = white;
			this.foldOutStyle.active.textColor = Color.red;
			this.foldOutStyle.focused.textColor = Color.red;
			this.foldOutStyle.hover.textColor = Color.red;
			this.normalButtonStyle = new GUIStyle(EditorStyles.toolbarButton);
			this.normalButtonStyle.fontSize = 15;
			this.normalButtonStyle.fontStyle = FontStyle.Normal;
			this.normalButtonStyle.border = new RectOffset(6, 6, 6, 6);
			this.normalButtonStyle.normal.textColor = new Color(0f, 0f, 0f, 1f);
			this.normalButtonStyle.active.textColor = Color.red;
			this.normalButtonStyle.focused.textColor = Color.red;
			this.normalButtonStyle.hover.textColor = Color.red;
			this.normalButtonStyle.onNormal.textColor = Color.black;
			this.normalButtonStyle.onActive.textColor = Color.red;
			this.normalButtonStyle.onFocused.textColor = Color.red;
			this.normalButtonStyle.onHover.textColor = Color.red;
			this.normalButtonStyle.fixedHeight = 60f;
			this.normalButtonStyle.fixedWidth = 300f;
			this.normalButtonStyle.padding = new RectOffset(0, 0, 0, 0);
			this.normalButtonStyle.margin = new RectOffset(0, 0, 0, 0);
			this.newButtonStyle = new GUIStyle(EditorStyles.toolbarButton);
			this.newButtonStyle.fontSize = 15;
			this.newButtonStyle.fontStyle = FontStyle.Normal;
			this.newButtonStyle.border = new RectOffset(6, 6, 6, 6);
			this.newButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_newPathButton", typeof(Texture2D));
			this.newButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_newPathButton_selected", typeof(Texture2D));
			this.newButtonStyle.fixedHeight = 90f;
			this.newButtonStyle.fixedWidth = 90f;
			this.newButtonStyle.padding = new RectOffset(0, 0, 0, 0);
			this.newButtonStyle.margin = new RectOffset(0, 0, 0, 0);
			GUIStyle guistyle = new GUIStyle(EditorStyles.toolbarButton);
			guistyle.fontSize = 15;
			guistyle.fontStyle = FontStyle.Normal;
			guistyle.border = new RectOffset(0, 0, 0, 0);
			guistyle.fixedHeight = 120f;
			guistyle.fixedWidth = 120f;
			guistyle.padding = new RectOffset(0, 0, 0, 0);
			guistyle.margin = new RectOffset(0, 0, 0, 0);
			this.invissibleButtonStyle = new GUIStyle(guistyle);
			this.invissibleButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_invissibleButton", typeof(Texture2D));
			this.invissibleButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_invissibleButton", typeof(Texture2D));
			this.findBonesButtonStyle = new GUIStyle(guistyle);
			this.findBonesButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_findBonesButton", typeof(Texture2D));
			this.findBonesButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_findBonesButton_selected", typeof(Texture2D));
			this.findBonesButtonStyle.fixedHeight = 90f;
			this.findBonesButtonStyle.fixedWidth = 90f;
			this.cleanBonesButtonStyle = new GUIStyle(guistyle);
			this.cleanBonesButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_cleanBonesButton", typeof(Texture2D));
			this.cleanBonesButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_cleanBonesButton_selected", typeof(Texture2D));
			this.cleanBonesButtonStyle.fixedHeight = 90f;
			this.cleanBonesButtonStyle.fixedWidth = 90f;
			this.createRagdollButtonStyle = new GUIStyle(guistyle);
			this.createRagdollButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_createRagdollButton", typeof(Texture2D));
			this.createRagdollButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_createRagdollButton_selected", typeof(Texture2D));
			this.createRagdollButtonStyle.fixedHeight = 90f;
			this.createRagdollButtonStyle.fixedWidth = 90f;
			this.removeRagdollButtonStyle = new GUIStyle(guistyle);
			this.removeRagdollButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_removeRagdollButton", typeof(Texture2D));
			this.removeRagdollButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_removeRagdollButton_selected", typeof(Texture2D));
			this.removeRagdollButtonStyle.fixedHeight = 90f;
			this.removeRagdollButtonStyle.fixedWidth = 90f;
			this.cleanScriptsButtonStyle = new GUIStyle(guistyle);
			this.cleanScriptsButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_cleanScriptsButton", typeof(Texture2D));
			this.cleanScriptsButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_cleanScriptsButton_selected", typeof(Texture2D));
			this.cleanScriptsButtonStyle.fixedHeight = 90f;
			this.cleanScriptsButtonStyle.fixedWidth = 90f;
			this.saveProfileButtonStyle = new GUIStyle(guistyle);
			this.saveProfileButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_saveProfileButton", typeof(Texture2D));
			this.saveProfileButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_saveProfileButton_selected", typeof(Texture2D));
			this.removeProfileButtonStyle = new GUIStyle(guistyle);
			this.removeProfileButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_removeProfileButton", typeof(Texture2D));
			this.removeProfileButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_removeProfileButton_selected", typeof(Texture2D));
			this.removeProfileButtonStyle.fixedHeight = 30f;
			this.removeProfileButtonStyle.fixedWidth = 30f;
			this.profileSelectedButtonStyle = new GUIStyle(guistyle);
			this.profileSelectedButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_profileSelectedButton", typeof(Texture2D));
			this.profileSelectedButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_profileSelectedButton_selected", typeof(Texture2D));
			this.profileSelectedButtonStyle.fixedHeight = 30f;
			this.profileSelectedButtonStyle.fixedWidth = 30f;
			this.profileNotSelectedButtonStyle = new GUIStyle(guistyle);
			this.profileNotSelectedButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_profileNotSelectedButton", typeof(Texture2D));
			this.profileNotSelectedButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_profileNotSelectedButton_selected", typeof(Texture2D));
			this.profileNotSelectedButtonStyle.fixedHeight = 30f;
			this.profileNotSelectedButtonStyle.fixedWidth = 30f;
			this.invissibleMiniButtonStyle = new GUIStyle(guistyle);
			this.invissibleMiniButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_invissibleButton", typeof(Texture2D));
			this.invissibleMiniButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_invissibleButton", typeof(Texture2D));
			this.invissibleMiniButtonStyle.fixedHeight = 30f;
			this.invissibleMiniButtonStyle.fixedWidth = 30f;
			this.characterSelectionButtonStyle = new GUIStyle(guistyle);
			this.characterSelectionButtonStyle.normal.background = (Texture2D)Resources.Load("EasyRagdollCreator_squaredButtonNoText", typeof(Texture2D));
			this.characterSelectionButtonStyle.active.background = (Texture2D)Resources.Load("EasyRagdollCreator_squaredButtonNoText_Selected", typeof(Texture2D));
			this.characterSelectionButtonStyle.fontSize = 15;
			this.characterSelectionButtonStyle.fontStyle = FontStyle.Normal;
			this.characterSelectionButtonStyle.normal.textColor = new Color(0f, 0f, 0f, 1f);
			this.characterSelectionButtonStyle.active.textColor = Color.red;
			this.characterSelectionButtonStyle.focused.textColor = Color.red;
			this.characterSelectionButtonStyle.hover.textColor = Color.red;
			this.characterSelectionButtonStyle.onNormal.textColor = Color.black;
			this.characterSelectionButtonStyle.onActive.textColor = Color.red;
			this.characterSelectionButtonStyle.onFocused.textColor = Color.red;
			this.characterSelectionButtonStyle.onHover.textColor = Color.red;
			this.characterSelectionButtonStyle.fixedHeight = 30f;
			this.characterSelectionButtonStyle.fixedWidth = 300f;
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00003AE8 File Offset: 0x00001CE8
		private void LoadDefaultProfile()
		{
			this.actualProfileBonesList.Clear();
			this.actualProfileBonesList.Add("hips");
			this.actualProfileBonesList.Add("spine1");
			this.actualProfileBonesList.Add("head");
			this.actualProfileBonesList.Add("leftupleg");
			this.actualProfileBonesList.Add("leftleg");
			this.actualProfileBonesList.Add("leftfoot");
			this.actualProfileBonesList.Add("leftarm");
			this.actualProfileBonesList.Add("leftforearm");
			this.actualProfileBonesList.Add("rightupleg");
			this.actualProfileBonesList.Add("rightleg");
			this.actualProfileBonesList.Add("rightfoot");
			this.actualProfileBonesList.Add("rightarm");
			this.actualProfileBonesList.Add("rightforearm");
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00003BD0 File Offset: 0x00001DD0
		private void TryToSetThisProfile()
		{
			if (this.profileList[this.selectedProfileIndex])
			{
				string[] array = this.profileList[this.selectedProfileIndex].text.Split(new char[]
				{
					'\n'
				});
				this.actualProfileBonesList.Clear();
				for (int i = 0; i < array.Length - 1; i++)
				{
					this.actualProfileBonesList.Add(this.RemoveStrangeCharacters(array[i + 1]));
				}
			}
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00003C50 File Offset: 0x00001E50
		private void SaveProfile(string newProfileName)
		{
			this.LoadProfiles();
			string path = AssetDatabase.GetAssetPath(Resources.Load("EasyRagdollCreator_logo")).Replace("EasyRagdollCreator_logo.png", "") + "Profiles/EasyRagdollCreator_Profile_" + (this.profileList.Count + 1).ToString() + ".txt";
			OwnRagdollBuilder.WriteString(path, newProfileName, false);
			OwnRagdollBuilder.WriteString(path, this.pelvis_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.middleSpine_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.head_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.leftHips_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.leftKnee_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.leftFoot_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.leftArm_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.leftElbow_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.rightHips_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.rightKnee_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.rightFoot_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.rightArm_ragdoll.gameObject.name, true);
			OwnRagdollBuilder.WriteString(path, this.rightElbow_ragdoll.gameObject.name, true);
			AssetDatabase.Refresh();
			this.LoadProfiles();
			AssetDatabase.Refresh();
			this.RepaintEditorWindow();
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00003DF0 File Offset: 0x00001FF0
		private void RemoveProfile(int index)
		{
			if (this.GetProfileNameByText(this.profileList[index].text) == "Default")
			{
				EditorUtility.DisplayDialog("WARNING", "Default profile cannot be removed", "CONTINUE");
				return;
			}
			List<string> list = new List<string>();
			for (int i = 0; i < this.profileList.Count; i++)
			{
				if (i != index)
				{
					list.Add(this.profileList[i].text);
				}
			}
			for (int j = 0; j < this.profileList.Count; j++)
			{
				FileUtil.DeleteFileOrDirectory(AssetDatabase.GetAssetPath(this.profileList[j]));
			}
			string text = AssetDatabase.GetAssetPath(Resources.Load("EasyRagdollCreator_logo"));
			text = text.Replace("EasyRagdollCreator_logo.png", "");
			for (int k = 0; k < list.Count; k++)
			{
				OwnRagdollBuilder.WriteString(text + "Profiles/EasyRagdollCreator_Profile_" + (k + 1).ToString() + ".txt", list[k], false);
			}
			AssetDatabase.Refresh();
			this.LoadProfiles();
			this.RepaintEditorWindow();
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00003F0C File Offset: 0x0000210C
		private void LoadProfiles()
		{
			this.profileList.Clear();
			bool flag = true;
			while (flag)
			{
				flag = false;
				TextAsset textAsset = Resources.Load("Profiles/EasyRagdollCreator_Profile_" + (this.profileList.Count + 1), typeof(TextAsset)) as TextAsset;
				if (textAsset)
				{
					this.profileList.Add(textAsset);
					flag = true;
				}
			}
			if (this.profileList.Count <= 0)
			{
				this.LoadDefaultProfile();
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00003F88 File Offset: 0x00002188
		private bool GetProfileAlreadyExists(string name)
		{
			for (int i = 0; i < this.profileList.Count; i++)
			{
				if (this.profileList[i] && this.GetProfileNameByText(this.profileList[i].text) == name)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00003FE0 File Offset: 0x000021E0
		private string RemoveStrangeCharacters(string s)
		{
			string text = "";
			foreach (char c in s)
			{
				if (c != '\n' && c != '\r')
				{
					text += c.ToString();
				}
			}
			return text;
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00004028 File Offset: 0x00002228
		private string GetProfileNameByText(string profile)
		{
			string[] array = profile.Split(new char[]
			{
				'\n'
			});
			return this.RemoveStrangeCharacters(array[0]);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00004050 File Offset: 0x00002250
		private static void WriteString(string path, string r, bool append)
		{
			StreamWriter streamWriter = new StreamWriter(path, append);
			streamWriter.WriteLine(r);
			streamWriter.Close();
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00004068 File Offset: 0x00002268
		private static string ReadString(string path)
		{
			string str = "";
			StreamReader streamReader = new StreamReader(path);
			string result = str + streamReader.ReadToEnd();
			streamReader.Close();
			return result;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00004094 File Offset: 0x00002294
		private void UpdateExtraTab()
		{
			if (this.lastSelectedButtonIndex == 1)
			{
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorFunctions.DrawEditorBox("Visualization");
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUI.BeginDisabledGroup(!Selection.activeGameObject);
				bool flag = this.showHelpers;
				this.showHelpers = EditorGUILayout.Toggle("Show helpers", this.showHelpers, new GUILayoutOption[0]);
				if (this.showHelpers != flag)
				{
					EditorFunctions.SetShowHelpersRecursively(Selection.activeGameObject.transform, this.showHelpers);
				}
				bool flag2 = this.showRenderers;
				this.showRenderers = EditorGUILayout.Toggle("Show renderers", this.showRenderers, new GUILayoutOption[0]);
				if (this.showRenderers != flag2)
				{
					EditorFunctions.SetShowRenderersRecursively(Selection.activeGameObject.transform, this.showRenderers);
				}
				this.showBonesCenter = EditorGUILayout.Toggle("Show bones center", this.showBonesCenter, new GUILayoutOption[0]);
				this.drawBonesRelationship = EditorGUILayout.Toggle("Draw relationships", this.drawBonesRelationship, new GUILayoutOption[0]);
				EditorGUI.EndDisabledGroup();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorFunctions.DrawEditorBox("Profiles");
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				GUILayout.BeginHorizontal(new GUILayoutOption[0]);
				EditorGUILayout.LabelField("New profile name", new GUILayoutOption[0]);
				this.newProfileName = EditorGUILayout.TextArea(this.newProfileName, new GUILayoutOption[]
				{
					GUILayout.Width(180f)
				});
				GUILayout.EndHorizontal();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				GUILayout.BeginHorizontal(new GUILayoutOption[0]);
				GUILayout.FlexibleSpace();
				if (GUILayout.Button(new GUIContent("", "Save the profile by name"), this.saveProfileButtonStyle, new GUILayoutOption[0]))
				{
					if (this.GetAllBonesSeemsToBeCorrect())
					{
						if (this.GetProfileAlreadyExists(this.newProfileName))
						{
							EditorUtility.DisplayDialog("WARNING", "\"" + this.newProfileName + "\" already exists as profile", "CONTINUE");
						}
						else
						{
							this.SaveProfile(this.newProfileName);
						}
					}
					else
					{
						EditorUtility.DisplayDialog("WARNING", "Some bones are missing", "CONTINUE");
					}
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				this.showProfiles = EditorGUILayout.Foldout(this.showProfiles, "Show profiles", this.foldOutStyle);
				if (this.showProfiles && this.profileList.Count > 0)
				{
					this.actualScrollViewForProfilesPosition = EditorGUILayout.BeginScrollView(this.actualScrollViewForProfilesPosition, new GUILayoutOption[0]);
					EditorGUILayout.Separator();
					EditorGUILayout.Separator();
					for (int i = 0; i < this.profileList.Count; i++)
					{
						if (this.profileList[i])
						{
							GUILayout.BeginHorizontal(new GUILayoutOption[0]);
							GUILayout.FlexibleSpace();
							GUILayout.Label(this.profileList[i].text.Split(new char[]
							{
								'\n'
							})[0], new GUILayoutOption[]
							{
								GUILayout.Width(300f)
							});
							GUILayout.FlexibleSpace();
							if (this.selectedProfileIndex == i)
							{
								if (GUILayout.Button(new GUIContent("", "Profile not selected"), this.profileSelectedButtonStyle, new GUILayoutOption[0]))
								{
								}
							}
							else if (GUILayout.Button(new GUIContent("", "Profile selected"), this.profileNotSelectedButtonStyle, new GUILayoutOption[0]))
							{
								this.selectedProfileIndex = i;
								this.TryToSetThisProfile();
							}
							if (i <= 5)
							{
								if (GUILayout.Button(new GUIContent("", ""), this.invissibleMiniButtonStyle, new GUILayoutOption[0]))
								{
								}
							}
							else if (GUILayout.Button(new GUIContent("", "Remove profile"), this.removeProfileButtonStyle, new GUILayoutOption[0]))
							{
								this.RemoveProfile(i);
							}
							GUILayout.FlexibleSpace();
							GUILayout.EndHorizontal();
						}
					}
					EditorGUILayout.EndScrollView();
					return;
				}
			}
			else
			{
				this.actualScrollViewForProfilesPosition = Vector2.zero;
			}
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00004454 File Offset: 0x00002654
		private void UpdateHomeTab()
		{
			if (this.lastSelectedButtonIndex == 0)
			{
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorFunctions.DrawEditorBox("Basic options");
				EditorGUILayout.BeginHorizontal(new GUILayoutOption[0]);
				GUILayout.FlexibleSpace();
				if (GUILayout.Button(new GUIContent("", "Find bones using the selected profile"), this.findBonesButtonStyle, new GUILayoutOption[0]))
				{
					if (Selection.activeGameObject)
					{
						this.FindBones();
						if (!this.GetAllBonesSeemsToBeCorrect())
						{
							EditorUtility.DisplayDialog("WARNING", "Some bones are not well configured, default profile will be selected. Remember, you have to select the character root. Besides you can select another profile or set the bones manually. In this case don't forget to save the new profile if everything is ok, so you can select it in the future for similar characters.", "CONTINUE");
							this.selectedProfileIndex = 0;
							this.LoadDefaultProfile();
							this.FindBones();
						}
					}
					else
					{
						EditorUtility.DisplayDialog("WARNING", "Nothing selected", "CONTINUE");
					}
				}
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				if (GUILayout.Button(new GUIContent("", "Clean detected bones"), this.cleanBonesButtonStyle, new GUILayoutOption[0]))
				{
					this.CleanBones();
				}
				if (!EditorFunctions.GetActualSelectionIsRagdollCharacter())
				{
					EditorGUILayout.Separator();
					EditorGUILayout.Separator();
					if (GUILayout.Button(new GUIContent("", "Create ragdoll"), this.createRagdollButtonStyle, new GUILayoutOption[0]))
					{
						if (this.GetAllBonesSeemsToBeCorrect())
						{
							this.CreateRagdoll();
						}
						else
						{
							EditorUtility.DisplayDialog("WARNING", "Some bones are missing", "CONTINUE");
						}
					}
				}
				if (EditorFunctions.GetActualSelectionIsRagdollCharacter())
				{
					EditorGUILayout.Separator();
					EditorGUILayout.Separator();
					if (GUILayout.Button(new GUIContent("", "Remove the actual ragdoll"), this.removeRagdollButtonStyle, new GUILayoutOption[0]))
					{
						this.RemoveRagdoll();
					}
				}
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				if (GUILayout.Button(new GUIContent("", "Clean Easy Ragdoll scripts from selected gameobject"), this.cleanScriptsButtonStyle, new GUILayoutOption[0]))
				{
					if (Selection.activeGameObject)
					{
						EditorFunctions.CleanScripts(Selection.activeGameObject.transform);
						EditorUtility.DisplayDialog("WARNING", "All scripts cleaned", "CONTINUE");
					}
					else
					{
						EditorUtility.DisplayDialog("WARNING", "Nothing selected", "CONTINUE");
					}
				}
				GUILayout.FlexibleSpace();
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUI.BeginDisabledGroup(EditorFunctions.GetActualSelectionIsRagdollCharacter());
				this.mecanimCharacter = EditorGUILayout.Toggle(new GUIContent("Mecanim character", "Add a component to handle mecanim animations and ragdoll at the same time"), this.mecanimCharacter, new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				this.totalMass = EditorGUILayout.Slider("Total mass", this.totalMass, 1f, 1000f, new GUILayoutOption[0]);
				this.configurableBones = EditorGUILayout.Toggle(new GUIContent("Configurable bones", "Create an independent structure to configure bones correctly"), this.configurableBones, new GUILayoutOption[0]);
				EditorGUI.EndDisabledGroup();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				this.autoCleanBonesOnCharacterChange = EditorGUILayout.Toggle(new GUIContent("Autoclean bones", "Autoclean bones on selection change"), this.autoCleanBonesOnCharacterChange, new GUILayoutOption[0]);
				EditorGUI.BeginDisabledGroup(!this.configurableBones);
				this.bonesNotAffectedByFatherOnEdit = EditorGUILayout.Toggle(new GUIContent("Independent edit", "Allows to edit each bone independently from its parent"), this.bonesNotAffectedByFatherOnEdit, new GUILayoutOption[0]);
				EditorGUI.EndDisabledGroup();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				string profileNameByText = this.GetProfileNameByText(this.profileList[this.selectedProfileIndex].text);
				EditorFunctions.DrawEditorBox("Selected profile: " + profileNameByText);
				if (Selection.activeGameObject)
				{
					EditorGUILayout.LabelField("Selected character: " + Selection.activeGameObject.name, new GUILayoutOption[0]);
				}
				else
				{
					EditorGUILayout.LabelField("Nothing selected", new GUILayoutOption[0]);
				}
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				this.actualScrollViewForBonesPosition = EditorGUILayout.BeginScrollView(this.actualScrollViewForBonesPosition, new GUILayoutOption[0]);
				this.pelvis_ragdoll = (EditorGUILayout.ObjectField("Pelvis", this.pelvis_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				EditorGUILayout.Separator();
				this.middleSpine_ragdoll = (EditorGUILayout.ObjectField("Middle spine", this.middleSpine_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				this.head_ragdoll = (EditorGUILayout.ObjectField("Head", this.head_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				EditorGUILayout.Separator();
				this.leftHips_ragdoll = (EditorGUILayout.ObjectField("Left hips", this.leftHips_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				this.leftKnee_ragdoll = (EditorGUILayout.ObjectField("Left knee", this.leftKnee_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				this.leftFoot_ragdoll = (EditorGUILayout.ObjectField("Left foot", this.leftFoot_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				EditorGUILayout.Separator();
				this.leftArm_ragdoll = (EditorGUILayout.ObjectField("Left arm", this.leftArm_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				this.leftElbow_ragdoll = (EditorGUILayout.ObjectField("Left elbow", this.leftElbow_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				EditorGUILayout.Separator();
				this.rightHips_ragdoll = (EditorGUILayout.ObjectField("Right hips", this.rightHips_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				this.rightKnee_ragdoll = (EditorGUILayout.ObjectField("Right knee", this.rightKnee_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				this.rightFoot_ragdoll = (EditorGUILayout.ObjectField("Right foot", this.rightFoot_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				EditorGUILayout.Separator();
				this.rightArm_ragdoll = (EditorGUILayout.ObjectField("Right arm", this.rightArm_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				this.rightElbow_ragdoll = (EditorGUILayout.ObjectField("Right elbow", this.rightElbow_ragdoll, typeof(Transform), true, new GUILayoutOption[0]) as Transform);
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.EndScrollView();
				return;
			}
			this.actualScrollViewForBonesPosition = Vector2.zero;
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00004A4C File Offset: 0x00002C4C
		private void RemoveRagdoll()
        {
            RemoveRagdollFrom(pelvis_ragdoll);
            RemoveRagdollFrom(middleSpine_ragdoll);
            RemoveRagdollFrom(head_ragdoll);
            RemoveRagdollFrom(leftHips_ragdoll);
            RemoveRagdollFrom(leftKnee_ragdoll);
            RemoveRagdollFrom(leftFoot_ragdoll);
            RemoveRagdollFrom(leftArm_ragdoll);
            RemoveRagdollFrom(leftElbow_ragdoll);
            RemoveRagdollFrom(rightHips_ragdoll);
            RemoveRagdollFrom(rightKnee_ragdoll);
            RemoveRagdollFrom(rightFoot_ragdoll);
            RemoveRagdollFrom(rightArm_ragdoll);
            RemoveRagdollFrom(rightElbow_ragdoll);

            Transform parentRagdollCharacter = Functions.GetParentRagdollCharacter(Selection.activeGameObject.transform);
			if (parentRagdollCharacter)
			{
				RagdollCharacter component = parentRagdollCharacter.GetComponent<RagdollCharacter>();
				if (component)
				{
					Transform transform = component.transform.Find("RagdollPhysics");
					if (transform)
					{
						Object.DestroyImmediate(transform.gameObject);
					}
					Object.DestroyImmediate(component);
				}
				MecanimCharacter component2 = parentRagdollCharacter.GetComponent<MecanimCharacter>();
				if (component2)
				{
					Object.DestroyImmediate(component2);
				}
			}
		}

        private void RemoveRagdollFrom(Transform transform)
        {
            if(transform == null)
                return;

            var charJoint = transform.GetComponent<Joint>();
            if(charJoint != null)
                DestroyImmediate(charJoint);
            var collidier = transform.GetComponent<Collider>();
            if (collidier != null)
                DestroyImmediate(collidier);
            var rigidbody = transform.GetComponent<Rigidbody>();
            if (rigidbody != null)
                DestroyImmediate(rigidbody);
        }

		// Token: 0x06000024 RID: 36 RVA: 0x00004AD8 File Offset: 0x00002CD8
		private void CleanBones()
		{
			this.pelvis_ragdoll = null;
			this.middleSpine_ragdoll = null;
			this.head_ragdoll = null;
			this.leftHips_ragdoll = null;
			this.leftKnee_ragdoll = null;
			this.leftFoot_ragdoll = null;
			this.rightHips_ragdoll = null;
			this.rightKnee_ragdoll = null;
			this.rightFoot_ragdoll = null;
			this.leftArm_ragdoll = null;
			this.leftElbow_ragdoll = null;
			this.rightArm_ragdoll = null;
			this.rightElbow_ragdoll = null;
			SceneView.RepaintAll();
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00004B48 File Offset: 0x00002D48
		private void FindBones()
		{
			this.pelvis_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[0]);
			this.middleSpine_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[1]);
			this.head_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[2]);
			this.leftHips_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[3]);
			this.leftKnee_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[4]);
			this.leftFoot_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[5]);
			this.leftArm_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[6]);
			this.leftElbow_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[7]);
			this.rightHips_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[8]);
			this.rightKnee_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[9]);
			this.rightFoot_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[10]);
			this.rightArm_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[11]);
			this.rightElbow_ragdoll = EditorFunctions.FindComponentInChildByStringChain(Selection.activeGameObject.transform, this.actualProfileBonesList[12]);
			this.CheckRagdollStability();
			SceneView.RepaintAll();
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00004D14 File Offset: 0x00002F14
		private void UpdateHelpTab()
		{
			if (this.lastSelectedButtonIndex == 2)
			{
				this.actualScrollViewForHelpPosition = EditorGUILayout.BeginScrollView(this.actualScrollViewForHelpPosition, new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorFunctions.DrawEditorBox("Basics");
				EditorStyles.label.normal.textColor = Color.black;
				EditorStyles.label.fontStyle = 0;
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Select the character root in the hierarchy", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Press FIND BONES button", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". If everything goes ok just press CREATE RAGDOLL", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  and it's is done. Just press PLAY to see the result", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  in realtime", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". In case of some missing bone there are some", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  existing profiles to try with them", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Besides bones can be set manually and then", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  save a new profile with a descriptive name", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Ragoll can be removed easily just pressing", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  REMOVE RAGDOLL button", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Default profile is compatible and general for", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  a lot of different type of characters", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Anyways there are preconfigured profiles for", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  known tools like 3D Studio Max, Mixamo, etc", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". If the character is created as Mecanim a new", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  component will be added to handle transitions", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  between animation state and ragdoll state. It's", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  still under construction, so just swithing off/on", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  is yet allowed", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Total bones's mass can be configured", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". If configurable bones is enabled then the ragdoll will", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  be created separately from the main skeleton hierarchy", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  to allow editing each joint and bone position. The", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  configurable hierarchy can be found inside the character", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  root called RagdollPhysics", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". If Autoclean bones is enabled the found bone list", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  will be cleaned on selection change", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". If Independent edit is enabled then each configurable bone", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  can be moved or rotated independently from its parent", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Pressing CLEAN SCRIPTS button all scripts will be removed", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  preserving all the ragdoll configuration.", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  This is useful to export the prefabs to another projects", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  where Easy Ragdoll Creator is not installed", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorFunctions.DrawEditorBox("Visualization");
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Helpers are useful to move the configurable joints and bones", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  To move bones transforms use directly the inspector", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Main character renderer can be hide to show the ragdoll", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  structure in a better way", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Bones center are very useful to know if the found bones are", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  the correct ones", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". The same happens with the relationships", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorFunctions.DrawEditorBox("Profiles");
				EditorGUILayout.LabelField(". One a character has its bones found a new profile can", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  be saved. It's very useful if there are too many characters", new GUILayoutOption[0]);
				EditorGUILayout.LabelField("  with the same structure", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". User created profiles can be removed", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorFunctions.DrawEditorBox("In the future");
				EditorGUILayout.Separator();
				EditorGUILayout.LabelField(". Expect more updates soon!", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorStyles.label.normal.textColor = Color.black;
				EditorStyles.label.fontStyle = FontStyle.Normal;
				EditorGUILayout.LabelField("[v1.0g]", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorStyles.label.normal.textColor = Color.black;
				EditorStyles.label.fontStyle = FontStyle.Italic;
				EditorGUILayout.LabelField("@Copyright Breaking The Code 2017-2018", new GUILayoutOption[0]);
				EditorGUILayout.Separator();
				EditorGUILayout.Separator();
				EditorGUILayout.EndScrollView();
				return;
			}
			this.actualScrollViewForHelpPosition = Vector2.zero;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000051A2 File Offset: 0x000033A2
		private void CreateRagdoll()
		{
            allRigidbodies = new List<Rigidbody>();
            allColliders = new List<Collider>();
            allJoint = new List<Joint>();

			this.CheckRagdollStability();
			this.CleanTheRagoll();
			this.BuildRagdollCapsuleCollliders();
			this.AddChestColliders();
			this.AddHeadCollider();
			this.BuildRagdollRigidbodies();
			this.BuildRagdollJoints();
			this.AdjustRagdollMass();
			this.AddComponents();
			this.PostProcessBones();
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000051E4 File Offset: 0x000033E4
		private void CleanTheRagoll()
		{
			IEnumerator enumerator = this.bones.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					OwnBoneInfo ownBoneInfo = (OwnBoneInfo)obj;
					if (ownBoneInfo.anchor)
					{
						Component[] componentsInChildren = ownBoneInfo.anchor.GetComponentsInChildren(typeof(Joint));
						for (int i = 0; i < componentsInChildren.Length; i++)
						{
							Object.DestroyImmediate((Joint)componentsInChildren[i]);
						}
						Component[] componentsInChildren2 = ownBoneInfo.anchor.GetComponentsInChildren(typeof(Rigidbody));
						for (int j = 0; j < componentsInChildren2.Length; j++)
						{
							Object.DestroyImmediate((Rigidbody)componentsInChildren2[j]);
						}
						Component[] componentsInChildren3 = ownBoneInfo.anchor.GetComponentsInChildren(typeof(Collider));
						for (int k = 0; k < componentsInChildren3.Length; k++)
						{
							Object.DestroyImmediate((Collider)componentsInChildren3[k]);
						}
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000052F4 File Offset: 0x000034F4
		private void BuildRagdollRigidbodies()
		{
			IEnumerator enumerator = this.bones.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					OwnBoneInfo ownBoneInfo = (OwnBoneInfo)obj;
					allRigidbodies.Add(ownBoneInfo.anchor.gameObject.AddComponent<Rigidbody>());
					ownBoneInfo.anchor.GetComponent<Rigidbody>().mass = ownBoneInfo.density;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00005370 File Offset: 0x00003570
		private void BuildRagdollJoints()
		{
			IEnumerator enumerator = this.bones.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					OwnBoneInfo ownBoneInfo = (OwnBoneInfo)obj;
					if (ownBoneInfo.parent != null)
					{
						CharacterJoint characterJoint = ownBoneInfo.anchor.gameObject.AddComponent<CharacterJoint>();
                        allJoint.Add(characterJoint);

						ownBoneInfo.joint = characterJoint;
						characterJoint.axis = OwnRagdollBuilder.ProcessDirectionVectorAxis(ownBoneInfo.anchor.InverseTransformDirection(ownBoneInfo.axis));
						characterJoint.swingAxis = OwnRagdollBuilder.ProcessDirectionVectorAxis(ownBoneInfo.anchor.InverseTransformDirection(ownBoneInfo.normalAxis));
						characterJoint.anchor = Vector3.zero;
						characterJoint.connectedBody = ownBoneInfo.parent.anchor.GetComponent<Rigidbody>();
						characterJoint.enablePreprocessing = false;
						SoftJointLimit softJointLimit = default(SoftJointLimit);
						softJointLimit.contactDistance = 0f;
						softJointLimit.limit = ownBoneInfo.minLimit;
						characterJoint.lowTwistLimit = softJointLimit;
						softJointLimit.limit = ownBoneInfo.maxLimit;
						characterJoint.highTwistLimit = softJointLimit;
						softJointLimit.limit = ownBoneInfo.swingLimit;
						characterJoint.swing1Limit = softJointLimit;
						softJointLimit.limit = 0f;
						characterJoint.swing2Limit = softJointLimit;
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000054BC File Offset: 0x000036BC
		private void AdjustRagdollMassRecursively(OwnBoneInfo bone)
		{
			float num = bone.anchor.GetComponent<Rigidbody>().mass;
			IEnumerator enumerator = bone.children.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					OwnBoneInfo ownBoneInfo = (OwnBoneInfo)obj;
					this.AdjustRagdollMassRecursively(ownBoneInfo);
					num += ownBoneInfo.summedMass;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			bone.summedMass = num;
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00005538 File Offset: 0x00003738
		private void AdjustRagdollMass()
		{
			this.AdjustRagdollMassRecursively(this.rootBone);
			float num = this.totalMass / this.rootBone.summedMass;
			IEnumerator enumerator = this.bones.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					((OwnBoneInfo)obj).anchor.GetComponent<Rigidbody>().mass *= num;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this.AdjustRagdollMassRecursively(this.rootBone);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000055CC File Offset: 0x000037CC
		private static void ProcessDirectionVector(Vector3 point, out int direction, out float distance)
		{
			direction = 0;
			if (Mathf.Abs(point[1]) > Mathf.Abs(point[0]))
			{
				direction = 1;
			}
			if (Mathf.Abs(point[2]) > Mathf.Abs(point[direction]))
			{
				direction = 2;
			}
			distance = point[direction];
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00005628 File Offset: 0x00003828
		private static Vector3 ProcessDirectionVectorAxis(Vector3 point)
		{
			int num = 0;
			float num2;
			OwnRagdollBuilder.ProcessDirectionVector(point, out num, out num2);
			Vector3 zero = Vector3.zero;
			if (num2 > 0f)
			{
				zero[num] = 1f;
			}
			else
			{
				zero[num] = -1f;
			}
			return zero;
		}

		// Token: 0x0600002F RID: 47 RVA: 0x0000566C File Offset: 0x0000386C
		private static int GetSmallestComponent(Vector3 point)
		{
			int num = 0;
			if (Mathf.Abs(point[1]) < Mathf.Abs(point[0]))
			{
				num = 1;
			}
			if (Mathf.Abs(point[2]) < Mathf.Abs(point[num]))
			{
				num = 2;
			}
			return num;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x000056B8 File Offset: 0x000038B8
		private static int GetLargestComponent(Vector3 point)
		{
			int num = 0;
			if (Mathf.Abs(point[1]) > Mathf.Abs(point[0]))
			{
				num = 1;
			}
			if (Mathf.Abs(point[2]) > Mathf.Abs(point[num]))
			{
				num = 2;
			}
			return num;
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00005704 File Offset: 0x00003904
		private static int GetSecondGetLargestComponent(Vector3 point)
		{
			int num = OwnRagdollBuilder.GetSmallestComponent(point);
			int num2 = OwnRagdollBuilder.GetLargestComponent(point);
			if (num < num2)
			{
				int num3 = num2;
				num2 = num;
				num = num3;
			}
			int result;
			if (num == 0 && num2 == 1)
			{
				result = 2;
			}
			else if (num == 0 && num2 == 2)
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00005744 File Offset: 0x00003944
		private Bounds LinkRagdoll(Bounds bounds, Transform relativeTo, Transform clipTransform, bool below)
		{
			int largestComponent = OwnRagdollBuilder.GetLargestComponent(bounds.size);
			if (Vector3.Dot(this.worldUp, relativeTo.TransformPoint(bounds.max)) > Vector3.Dot(this.worldUp, relativeTo.TransformPoint(bounds.min)) == below)
			{
				Vector3 min = bounds.min;
				min[largestComponent] = relativeTo.InverseTransformPoint(clipTransform.position)[largestComponent];
				bounds.min = min;
			}
			else
			{
				Vector3 max = bounds.max;
				max[largestComponent] = relativeTo.InverseTransformPoint(clipTransform.position)[largestComponent];
				bounds.max = max;
			}
			return bounds;
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000057F4 File Offset: 0x000039F4
		private Bounds GetChestBounds(Transform relativeTo)
		{
			Bounds result = default(Bounds);
			result.Encapsulate(relativeTo.InverseTransformPoint(this.leftHips_ragdoll.position));
			result.Encapsulate(relativeTo.InverseTransformPoint(this.rightHips_ragdoll.position));
			result.Encapsulate(relativeTo.InverseTransformPoint(this.leftArm_ragdoll.position));
			result.Encapsulate(relativeTo.InverseTransformPoint(this.rightArm_ragdoll.position));
			Vector3 size = result.size;
			size[OwnRagdollBuilder.GetSmallestComponent(result.size)] = size[OwnRagdollBuilder.GetLargestComponent(result.size)] / 2f;
			result.size = size;
			return result;
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000058A8 File Offset: 0x00003AA8
		private void AddChestColliders()
		{
			if (this.middleSpine_ragdoll != null && this.pelvis_ragdoll != null)
			{
				Bounds bounds = this.LinkRagdoll(this.GetChestBounds(this.pelvis_ragdoll), this.pelvis_ragdoll, this.middleSpine_ragdoll, false);
				
                //if (pelvis_ragdoll.localEulerAngles.magnitude > 1)
                //{
                //    GameObject pelvisProxy = new GameObject("pelvisProxy");
                //    pelvisProxy.transform.SetParent(pelvis_ragdoll);
                //    pelvisProxy.transform.localEulerAngles = new Vector3(-pelvis_ragdoll.localEulerAngles.x,
                //        -pelvis_ragdoll.localEulerAngles.y,
                //        -pelvis_ragdoll.localEulerAngles.z);
                //    pelvisProxy.transform.localPosition = Vector3.zero;
                //    pelvis_ragdoll = pelvisProxy.transform;
                //    bounds.center = new Vector3(0, bounds.center.y, bounds.center.z);
                //    bounds.size = bounds.size * 0.75f;
                //}

                BoxCollider boxCollider = this.pelvis_ragdoll.gameObject.AddComponent<BoxCollider>();
                allColliders.Add(boxCollider);
				boxCollider.center = bounds.center;
				boxCollider.size = bounds.size;
				bounds = this.LinkRagdoll(this.GetChestBounds(this.middleSpine_ragdoll), this.middleSpine_ragdoll, this.middleSpine_ragdoll, true);
				BoxCollider boxCollider2 = this.middleSpine_ragdoll.gameObject.AddComponent<BoxCollider>();
                allColliders.Add(boxCollider2);
                boxCollider2.center = bounds.center;
				boxCollider2.size = bounds.size;
				return;
			}
			Bounds bounds2 = default(Bounds);
			bounds2.Encapsulate(this.pelvis_ragdoll.InverseTransformPoint(this.leftHips_ragdoll.position));
			bounds2.Encapsulate(this.pelvis_ragdoll.InverseTransformPoint(this.rightHips_ragdoll.position));
			bounds2.Encapsulate(this.pelvis_ragdoll.InverseTransformPoint(this.leftArm_ragdoll.position));
			bounds2.Encapsulate(this.pelvis_ragdoll.InverseTransformPoint(this.rightArm_ragdoll.position));
			Vector3 size = bounds2.size;
			size[OwnRagdollBuilder.GetSmallestComponent(bounds2.size)] = size[OwnRagdollBuilder.GetLargestComponent(bounds2.size)] / 2f;
			BoxCollider boxCollider3 = this.pelvis_ragdoll.gameObject.AddComponent<BoxCollider>();
			boxCollider3.center = bounds2.center;
			boxCollider3.size = size;
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00005A40 File Offset: 0x00003C40
		private static Transform FindComponentInChildByTwoStringChains(Transform mainTransform, string chainA, string chainB)
		{
			Transform result = null;
			foreach (object obj in mainTransform)
			{
				Transform transform = (Transform)obj;
				if (transform.name.ToLower().Contains(chainA.ToLower()) && transform.name.ToLower().Contains(chainB.ToLower()))
				{
					return transform;
				}
				result = OwnRagdollBuilder.FindComponentInChildByTwoStringChains(transform, chainA, chainB);
			}
			return result;
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00005AD4 File Offset: 0x00003CD4
		private string CheckRagdollStability()
		{
			this.BuildRagdollBones();
			Hashtable hashtable = new Hashtable();
			IEnumerator enumerator = this.bones.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					OwnBoneInfo ownBoneInfo = (OwnBoneInfo)obj;
					if (ownBoneInfo.anchor)
					{
						if (hashtable[ownBoneInfo.anchor] != null)
						{
							OwnBoneInfo ownBoneInfo2 = (OwnBoneInfo)hashtable[ownBoneInfo.anchor];
							return string.Format("{0} and {1} may not be assigned to the same bone.", ownBoneInfo.name, ownBoneInfo2.name);
						}
						hashtable[ownBoneInfo.anchor] = ownBoneInfo;
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			IEnumerator enumerator2 = this.bones.GetEnumerator();
			try
			{
				while (enumerator2.MoveNext())
				{
					object obj2 = enumerator2.Current;
					OwnBoneInfo ownBoneInfo3 = (OwnBoneInfo)obj2;
					if (ownBoneInfo3.anchor == null)
					{
						return string.Format("{0} has not been assigned yet.\n", ownBoneInfo3.name);
					}
				}
			}
			finally
			{
				IDisposable disposable2;
				if ((disposable2 = (enumerator2 as IDisposable)) != null)
				{
					disposable2.Dispose();
				}
			}
			return "";
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00005C00 File Offset: 0x00003E00
		private void SpoilRagdollVectors(out Vector3 normalCompo, out Vector3 tangentCompo, Vector3 outwardDir, Vector3 outwardNormal)
		{
			outwardNormal = outwardNormal.normalized;
			normalCompo = outwardNormal * Vector3.Dot(outwardDir, outwardNormal);
			tangentCompo = outwardDir - normalCompo;
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00005C34 File Offset: 0x00003E34
		private void BuildRagdollAxis()
		{
			if (this.head_ragdoll != null && this.pelvis_ragdoll != null)
			{
				this.up = OwnRagdollBuilder.ProcessDirectionVectorAxis(this.pelvis_ragdoll.InverseTransformPoint(this.head_ragdoll.position));
			}
			if (this.rightElbow_ragdoll != null && this.pelvis_ragdoll != null)
			{
				Vector3 vector;
				Vector3 point;
				this.SpoilRagdollVectors(out vector, out point, this.pelvis_ragdoll.InverseTransformPoint(this.rightElbow_ragdoll.position), this.up);
				this.right = OwnRagdollBuilder.ProcessDirectionVectorAxis(point);
			}
			this.forward = Vector3.Cross(this.right, this.up);
			if (this.flipForward)
			{
				this.forward = -this.forward;
			}
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00005CFC File Offset: 0x00003EFC
		private void BuildRagdollBones()
		{
			if (this.pelvis_ragdoll)
			{
				this.worldRight = this.pelvis_ragdoll.TransformDirection(this.right);
				this.worldUp = this.pelvis_ragdoll.TransformDirection(this.up);
				this.worldForward = this.pelvis_ragdoll.TransformDirection(this.forward);
			}
			this.bones.Clear();
			this.rootBone = new OwnBoneInfo();
			this.rootBone.name = "Pelvis";
			this.rootBone.anchor = this.pelvis_ragdoll;
			this.rootBone.parent = null;
			this.rootBone.density = 2.5f;
			this.bones.Add(this.rootBone);
			this.AddRagdollMirrorJoints("Hips", this.leftHips_ragdoll, this.rightHips_ragdoll, "Pelvis", this.worldRight, this.worldForward, -20f, 70f, 30f, typeof(CapsuleCollider), 0.3f, 1.5f);
			this.AddRagdollMirrorJoints("Knee", this.leftKnee_ragdoll, this.rightKnee_ragdoll, "Hips", this.worldRight, this.worldForward, -80f, 0f, 0f, typeof(CapsuleCollider), 0.25f, 1.5f);
			this.AddRagdollJoint("Middle Spine", this.middleSpine_ragdoll, "Pelvis", this.worldRight, this.worldForward, -20f, 20f, 10f, null, 1f, 2.5f);
			this.AddRagdollMirrorJoints("Arm", this.leftArm_ragdoll, this.rightArm_ragdoll, "Middle Spine", this.worldUp, this.worldForward, -70f, 10f, 50f, typeof(CapsuleCollider), 0.25f, 1f);
			this.AddRagdollMirrorJoints("Elbow", this.leftElbow_ragdoll, this.rightElbow_ragdoll, "Arm", this.worldForward, this.worldUp, -90f, 0f, 0f, typeof(CapsuleCollider), 0.2f, 1f);
			this.AddRagdollJoint("Head", this.head_ragdoll, "Middle Spine", this.worldRight, this.worldForward, -40f, 25f, 25f, null, 1f, 1f);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00005F64 File Offset: 0x00004164
		private OwnBoneInfo FindRagdollBone(string name)
		{
			IEnumerator enumerator = this.bones.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					OwnBoneInfo ownBoneInfo = (OwnBoneInfo)obj;
					if (ownBoneInfo.name == name)
					{
						return ownBoneInfo;
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			return null;
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00005FCC File Offset: 0x000041CC
		private void AddRagdollMirrorJoints(string name, Transform leftAnchor, Transform rightAnchor, string parent, Vector3 worldTwistAxis, Vector3 worldSwingAxis, float minLimit, float maxLimit, float swingLimit, Type colliderType, float radiusScale, float density)
		{
			this.AddRagdollJoint("Left " + name, leftAnchor, parent, worldTwistAxis, worldSwingAxis, minLimit, maxLimit, swingLimit, colliderType, radiusScale, density);
			this.AddRagdollJoint("Right " + name, rightAnchor, parent, worldTwistAxis, worldSwingAxis, minLimit, maxLimit, swingLimit, colliderType, radiusScale, density);
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00006024 File Offset: 0x00004224
		private void AddRagdollJoint(string name, Transform anchor, string parent, Vector3 worldTwistAxis, Vector3 worldSwingAxis, float minLimit, float maxLimit, float swingLimit, Type colliderType, float radiusScale, float density)
		{
			OwnBoneInfo ownBoneInfo = new OwnBoneInfo();
			ownBoneInfo.name = name;
			ownBoneInfo.anchor = anchor;
			ownBoneInfo.axis = worldTwistAxis;
			ownBoneInfo.normalAxis = worldSwingAxis;
			ownBoneInfo.minLimit = minLimit;
			ownBoneInfo.maxLimit = maxLimit;
			ownBoneInfo.swingLimit = swingLimit;
			ownBoneInfo.density = density;
			ownBoneInfo.colliderType = colliderType;
			ownBoneInfo.radiusScale = radiusScale;
			if (this.FindRagdollBone(parent) != null)
			{
				ownBoneInfo.parent = this.FindRagdollBone(parent);
			}
			else if (name.StartsWith("Left"))
			{
				ownBoneInfo.parent = this.FindRagdollBone("Left " + parent);
			}
			else if (name.StartsWith("Right"))
			{
				ownBoneInfo.parent = this.FindRagdollBone("Right " + parent);
			}
			ownBoneInfo.parent.children.Add(ownBoneInfo);
			this.bones.Add(ownBoneInfo);
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00006108 File Offset: 0x00004308
		private void BuildRagdollCapsuleCollliders()
		{
			IEnumerator enumerator = this.bones.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					OwnBoneInfo ownBoneInfo = (OwnBoneInfo)obj;
					if (ownBoneInfo.colliderType == typeof(CapsuleCollider))
					{
						int num;
						float num2;
						if (ownBoneInfo.children.Count == 1)
						{
							Vector3 position = ((OwnBoneInfo)ownBoneInfo.children[0]).anchor.position;
							OwnRagdollBuilder.ProcessDirectionVector(ownBoneInfo.anchor.InverseTransformPoint(position), out num, out num2);
						}
						else
						{
							Vector3 vector = ownBoneInfo.anchor.position - ownBoneInfo.parent.anchor.position + ownBoneInfo.anchor.position;
							OwnRagdollBuilder.ProcessDirectionVector(ownBoneInfo.anchor.InverseTransformPoint(vector), out num, out num2);
							if (ownBoneInfo.anchor.GetComponentsInChildren(typeof(Transform)).Length > 1)
							{
								Bounds bounds = default(Bounds);
								foreach (Transform transform in ownBoneInfo.anchor.GetComponentsInChildren(typeof(Transform)))
								{
									bounds.Encapsulate(ownBoneInfo.anchor.InverseTransformPoint(transform.position));
								}
								if (num2 > 0f)
								{
									num2 = bounds.max[num];
								}
								else
								{
									num2 = bounds.min[num];
								}
							}
						}
						CapsuleCollider capsuleCollider = ownBoneInfo.anchor.gameObject.AddComponent<CapsuleCollider>();
                        allColliders.Add(capsuleCollider);
						capsuleCollider.direction = num;
						Vector3 zero = Vector3.zero;
						zero[num] = num2 * 0.5f;
						capsuleCollider.center = zero;
						capsuleCollider.height = Mathf.Abs(num2);
						capsuleCollider.radius = Mathf.Abs(num2 * ownBoneInfo.radiusScale);
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00006308 File Offset: 0x00004508
		private void AddHeadCollider()
		{
			if (this.head_ragdoll.GetComponent<Collider>())
			{
				Object.Destroy(this.head_ragdoll.GetComponent<Collider>());
			}
			float num = Vector3.Distance(this.leftArm_ragdoll.transform.position, this.rightArm_ragdoll.transform.position);
			num /= 4f;
			SphereCollider sphereCollider = this.head_ragdoll.gameObject.AddComponent<SphereCollider>();
            allColliders.Add(sphereCollider);
            sphereCollider.radius = num * 2.5f;
			Vector3 zero = Vector3.zero;

            int num2;
			float num3;
			OwnRagdollBuilder.ProcessDirectionVector(this.head_ragdoll.InverseTransformPoint(this.pelvis_ragdoll.position), out num2, out num3);
			if (num3 > 0f)
			{
				zero[num2] = -num;
			}
			else
			{
				zero[num2] = num;
            }
            zero.y += 0.12f;
            zero.x -= 0.12f;
            sphereCollider.center = zero;
		}

		// Token: 0x0600003F RID: 63 RVA: 0x000063C4 File Offset: 0x000045C4
		private bool GetAllBonesSeemsToBeCorrect()
		{
			return this.pelvis_ragdoll && this.middleSpine_ragdoll && this.head_ragdoll && this.leftHips_ragdoll && this.leftKnee_ragdoll && this.leftFoot_ragdoll && this.rightHips_ragdoll && this.rightKnee_ragdoll && this.rightFoot_ragdoll && this.leftArm_ragdoll && this.leftElbow_ragdoll && this.rightArm_ragdoll && this.rightElbow_ragdoll;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00006498 File Offset: 0x00004698
		private void PostProcessBones()
		{
			if (this.configurableBones)
			{
				GameObject gameObject = Functions.GetParentRagdollCharacter(Selection.activeGameObject.transform).gameObject;
				GameObject gameObject2 = Object.Instantiate<GameObject>(gameObject);
				gameObject2.name = "RagdollPhysics";
				OwnRagdollBuilder.RemoveAllInsteadRagdollBones(gameObject2.transform);
				OwnRagdollBuilder.SetLinkedBonesRecursively(gameObject2.transform, gameObject.transform);
				gameObject2.transform.parent = gameObject.transform;
			}
			if (this.mecanimCharacter)
			{
				Functions.GetParentRagdollCharacter(Selection.activeGameObject.transform).gameObject.AddComponent<MecanimCharacter>();
			}
		}

		// Token: 0x06000041 RID: 65 RVA: 0x0000652C File Offset: 0x0000472C
		private static void SetLinkedBonesRecursively(Transform current, Transform original)
		{
			for (int i = 0; i < current.childCount; i++)
			{
				OwnRagdollBuilder.SetLinkedBonesRecursively(current.GetChild(i), original.GetChild(i));
			}
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00006574 File Offset: 0x00004774
		private static void RemoveAllInsteadRagdollBones(Transform current)
		{
			if (current.GetComponent<Renderer>())
			{
				Object.DestroyImmediate(current.GetComponent<Renderer>());
			}
			if (current.GetComponent<Animator>())
			{
				Object.DestroyImmediate(current.GetComponent<Animator>());
			}
			for (int i = 0; i < current.childCount; i++)
			{
				OwnRagdollBuilder.RemoveAllInsteadRagdollBones(current.GetChild(i));
			}
		}

        private List<Rigidbody> allRigidbodies;
        private List<Collider> allColliders;
        private List<Joint> allJoint;

		// Token: 0x06000043 RID: 67 RVA: 0x000065D0 File Offset: 0x000047D0
		private void AddComponents()
		{
			if (!Selection.activeGameObject)
			{
				return;
			}
			var ragdollChar = Selection.activeGameObject.AddComponent<RagdollCharacter>();
			this.ragdollBonesTransformsOriginalList.Clear();
			this.AddRagdollBone(this.pelvis_ragdoll);
			this.AddRagdollBone(this.middleSpine_ragdoll);
			this.AddRagdollBone(this.head_ragdoll);
			this.AddRagdollBone(this.leftHips_ragdoll);
			this.AddRagdollBone(this.leftKnee_ragdoll);
			this.AddRagdollBone(this.leftFoot_ragdoll);
			this.AddRagdollBone(this.rightHips_ragdoll);
			this.AddRagdollBone(this.rightKnee_ragdoll);
			this.AddRagdollBone(this.rightFoot_ragdoll);
			this.AddRagdollBone(this.leftArm_ragdoll);
			this.AddRagdollBone(this.leftElbow_ragdoll);
			this.AddRagdollBone(this.rightArm_ragdoll);
			this.AddRagdollBone(this.rightElbow_ragdoll);
            ragdollChar.SetUpRagdoll(allColliders.ToArray(), allRigidbodies.ToArray(), allJoint.ToArray());

        }

		// Token: 0x06000044 RID: 68 RVA: 0x000066A8 File Offset: 0x000048A8
		private void AddRagdollBone(Transform bone)
		{
			this.ragdollBonesTransformsOriginalList.Add(bone.transform);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00006704 File Offset: 0x00004904
		[MenuItem("Window/Breaking The Code/Easy Ragdoll Creator")]
		private static void Init()
		{
			OwnRagdollBuilder ownRagdollBuilder = (OwnRagdollBuilder)EditorWindow.GetWindow(typeof(OwnRagdollBuilder));
			ownRagdollBuilder.titleContent = new GUIContent("Easy Ragdoll Creator");
			ownRagdollBuilder.minSize = new Vector2(400f, 600f);
			ownRagdollBuilder.maxSize = new Vector2(400f, 9999f);
			ownRagdollBuilder.Show();
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00006764 File Offset: 0x00004964
		private void OnEnable()
		{
			if (this.firstTime)
			{
				this.firstTime = false;
				OwnRagdollBuilder.Singleton = this;
				this.window = (OwnRagdollBuilder)EditorWindow.GetWindow(typeof(OwnRagdollBuilder));
				SceneView.onSceneGUIDelegate = (SceneView.OnSceneFunc)Delegate.Combine(SceneView.onSceneGUIDelegate, new SceneView.OnSceneFunc(this.OnSceneGUI));
				EditorApplication.update = (EditorApplication.CallbackFunction)Delegate.Combine(EditorApplication.update, new EditorApplication.CallbackFunction(this.Update));
				this.LoadDefaultProfile();
				this.LoadProfiles();
			}
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000067EC File Offset: 0x000049EC
		private void OnDestroy()
		{
			OwnRagdollBuilder.Singleton = null;
			this.CleanBones();
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000067FA File Offset: 0x000049FA
		private void OnFocus()
		{
			this.weHaveTheFocus = true;
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000067FA File Offset: 0x000049FA
		private void OnLostFocus()
		{
			this.weHaveTheFocus = true;
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00006804 File Offset: 0x00004A04
		public void OnSceneGUI(SceneView scnView)
		{
			Color color = Handles.color;
			Handles.color = Color.red;
			if (this.showBonesCenter)
			{
				this.DrawBone(this.pelvis_ragdoll, true, 0.1f);
				this.DrawBone(this.middleSpine_ragdoll, true, 0.1f);
				this.DrawBone(this.head_ragdoll, true, 0.1f);
				this.DrawBone(this.leftHips_ragdoll, true, 0.1f);
				this.DrawBone(this.leftKnee_ragdoll, true, 0.1f);
				this.DrawBone(this.leftFoot_ragdoll, true, 0.1f);
				this.DrawBone(this.rightHips_ragdoll, true, 0.1f);
				this.DrawBone(this.rightKnee_ragdoll, true, 0.1f);
				this.DrawBone(this.rightFoot_ragdoll, true, 0.1f);
				this.DrawBone(this.leftArm_ragdoll, true, 0.1f);
				this.DrawBone(this.leftElbow_ragdoll, true, 0.1f);
				this.DrawBone(this.rightArm_ragdoll, true, 0.1f);
				this.DrawBone(this.rightElbow_ragdoll, true, 0.1f);
			}
			if (this.drawBonesRelationship)
			{
				Handles.color = Color.blue;
				this.DrawBoneRelationship(this.head_ragdoll, this.middleSpine_ragdoll);
				this.DrawBoneRelationship(this.head_ragdoll, this.leftArm_ragdoll);
				this.DrawBoneRelationship(this.head_ragdoll, this.rightArm_ragdoll);
				this.DrawBoneRelationship(this.middleSpine_ragdoll, this.pelvis_ragdoll);
				this.DrawBoneRelationship(this.pelvis_ragdoll, this.leftHips_ragdoll);
				this.DrawBoneRelationship(this.leftHips_ragdoll, this.leftKnee_ragdoll);
				this.DrawBoneRelationship(this.leftKnee_ragdoll, this.leftFoot_ragdoll);
				this.DrawBoneRelationship(this.pelvis_ragdoll, this.rightHips_ragdoll);
				this.DrawBoneRelationship(this.rightHips_ragdoll, this.rightKnee_ragdoll);
				this.DrawBoneRelationship(this.rightKnee_ragdoll, this.rightFoot_ragdoll);
				this.DrawBoneRelationship(this.middleSpine_ragdoll, this.leftArm_ragdoll);
				this.DrawBoneRelationship(this.leftArm_ragdoll, this.leftElbow_ragdoll);
				this.DrawBoneRelationship(this.middleSpine_ragdoll, this.rightArm_ragdoll);
				this.DrawBoneRelationship(this.rightArm_ragdoll, this.rightElbow_ragdoll);
				Handles.color = color;
			}
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00006A30 File Offset: 0x00004C30
		private void OnGUI()
		{
			Color color = GUI.color;
			Color textColor = EditorStyles.label.normal.textColor;
			FontStyle fontStyle = EditorStyles.label.fontStyle;
			Color white = Color.white;
			ColorUtility.TryParseHtmlString(Defines.LABELS_TEXT_COLOR, out white);
			EditorStyles.label.normal.textColor = white;
			EditorStyles.label.fontStyle = FontStyle.Bold;
			if (!this.weHaveTheFocus)
			{
				return;
			}
			Texture2D texture2D = (Texture2D)Resources.Load("EasyRagdollCreator_whiteBG", typeof(Texture2D));
			GUILayout.BeginArea(new Rect(0f, 0f, 4800f, 600f), texture2D);
			GUILayout.EndArea();
			Texture2D texture2D2 = (Texture2D)Resources.Load("EasyRagdollCreator_logo", typeof(Texture2D));
			GUILayout.BeginArea(new Rect(0f, 0f, 400f, 200f), texture2D2);
			GUILayout.EndArea();
			Texture2D texture2D3 = (Texture2D)Resources.Load("EasyRagdollCreator_mainBG", typeof(Texture2D));
			GUILayout.BeginArea(new Rect(0f, 94f, 1920f, 1080f), texture2D3);
			GUILayout.EndArea();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			EditorGUILayout.Separator();
			this.CreateCustomStyles();
			List<Texture2D> list = new List<Texture2D>();
			list.Add((Texture2D)Resources.Load("EasyRagdollCreator_toolbar_mainButton", typeof(Texture2D)));
			list.Add((Texture2D)Resources.Load("EasyRagdollCreator_toolbar_extraButton", typeof(Texture2D)));
			list.Add((Texture2D)Resources.Load("EasyRagdollCreator_toolbar_helpButton", typeof(Texture2D)));
			this.lastSelectedButtonIndex = GUILayout.Toolbar(this.lastSelectedButtonIndex, list.ToArray(), this.toolbarStyle, new GUILayoutOption[0]);
			GUI.color = color;
			this.UpdateHomeTab();
			this.UpdateExtraTab();
			this.UpdateHelpTab();
			EditorStyles.label.normal.textColor = textColor;
			EditorStyles.label.fontStyle = fontStyle;
			if (GUI.changed)
			{
				SceneView.RepaintAll();
			}
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00006C75 File Offset: 0x00004E75
		private void RepaintEditorWindow()
		{
			this.window.Repaint();
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00006C84 File Offset: 0x00004E84
		private void Update()
		{
			if (Selection.activeGameObject != this.lastSelectedActiveGameObject)
			{
				this.lastSelectedActiveGameObject = Selection.activeGameObject;
				if (this.autoCleanBonesOnCharacterChange)
				{
					this.CleanBones();
				}
				this.RepaintEditorWindow();
			}
			if (this.previousSelectedMainButton != this.lastSelectedButtonIndex)
			{
				this.previousSelectedMainButton = this.lastSelectedButtonIndex;
				this.RepaintEditorWindow();
			}
		}

		// Token: 0x04000004 RID: 4
		private GUIStyle toolbarStyle;

		// Token: 0x04000005 RID: 5
		private GUIStyle foldOutStyle;

		// Token: 0x04000006 RID: 6
		private GUIStyle normalButtonStyle;

		// Token: 0x04000007 RID: 7
		private GUIStyle characterSelectionButtonStyle;

		// Token: 0x04000008 RID: 8
		private GUIStyle newButtonStyle;

		// Token: 0x04000009 RID: 9
		private GUIStyle invissibleButtonStyle;

		// Token: 0x0400000A RID: 10
		private GUIStyle findBonesButtonStyle;

		// Token: 0x0400000B RID: 11
		private GUIStyle cleanBonesButtonStyle;

		// Token: 0x0400000C RID: 12
		private GUIStyle createRagdollButtonStyle;

		// Token: 0x0400000D RID: 13
		private GUIStyle removeRagdollButtonStyle;

		// Token: 0x0400000E RID: 14
		private GUIStyle cleanScriptsButtonStyle;

		// Token: 0x0400000F RID: 15
		private GUIStyle saveProfileButtonStyle;

		// Token: 0x04000010 RID: 16
		private GUIStyle removeProfileButtonStyle;

		// Token: 0x04000011 RID: 17
		private GUIStyle profileSelectedButtonStyle;

		// Token: 0x04000012 RID: 18
		private GUIStyle profileNotSelectedButtonStyle;

		// Token: 0x04000013 RID: 19
		private GUIStyle invissibleMiniButtonStyle;

		// Token: 0x04000014 RID: 20
		private bool showProfiles = true;

		// Token: 0x04000015 RID: 21
		private int selectedProfileIndex;

		// Token: 0x04000016 RID: 22
		private List<TextAsset> profileList = new List<TextAsset>();

		// Token: 0x04000017 RID: 23
		private string newProfileName = "New profile";

		// Token: 0x04000018 RID: 24
		private List<string> actualProfileBonesList = new List<string>();

		// Token: 0x04000019 RID: 25
		public bool showHelpers;

		// Token: 0x0400001A RID: 26
		public bool showRenderers = true;

		// Token: 0x0400001B RID: 27
		public bool showBonesCenter = true;

		// Token: 0x0400001C RID: 28
		public bool drawBonesRelationship = true;

		// Token: 0x0400001D RID: 29
		public int lastSelectedButtonIndex;

		// Token: 0x0400001E RID: 30
		public bool autoCleanBonesOnCharacterChange;

		// Token: 0x0400001F RID: 31
		public bool bonesNotAffectedByFatherOnEdit;

		// Token: 0x04000020 RID: 32
		public Transform pelvis_ragdoll;

		// Token: 0x04000021 RID: 33
		public Transform middleSpine_ragdoll;

		// Token: 0x04000022 RID: 34
		public Transform head_ragdoll;

		// Token: 0x04000023 RID: 35
		public Transform leftHips_ragdoll;

		// Token: 0x04000024 RID: 36
		public Transform leftKnee_ragdoll;

		// Token: 0x04000025 RID: 37
		public Transform leftFoot_ragdoll;

		// Token: 0x04000026 RID: 38
		public Transform rightHips_ragdoll;

		// Token: 0x04000027 RID: 39
		public Transform rightKnee_ragdoll;

		// Token: 0x04000028 RID: 40
		public Transform rightFoot_ragdoll;

		// Token: 0x04000029 RID: 41
		public Transform leftArm_ragdoll;

		// Token: 0x0400002A RID: 42
		public Transform leftElbow_ragdoll;

		// Token: 0x0400002B RID: 43
		public Transform rightArm_ragdoll;

		// Token: 0x0400002C RID: 44
		public Transform rightElbow_ragdoll;

		// Token: 0x0400002D RID: 45
		public bool mecanimCharacter;

		// Token: 0x0400002E RID: 46
		public float totalMass = 80f;

		// Token: 0x0400002F RID: 47
		public bool configurableBones;

		// Token: 0x04000030 RID: 48
		private Vector3 right = Vector3.right;

		// Token: 0x04000031 RID: 49
		private Vector3 up = Vector3.up;

		// Token: 0x04000032 RID: 50
		private Vector3 forward = Vector3.forward;

		// Token: 0x04000033 RID: 51
		private Vector3 worldRight = Vector3.right;

		// Token: 0x04000034 RID: 52
		private Vector3 worldUp = Vector3.up;

		// Token: 0x04000035 RID: 53
		private Vector3 worldForward = Vector3.forward;

		// Token: 0x04000036 RID: 54
		public bool flipForward;

		// Token: 0x04000037 RID: 55
		private ArrayList bones = new ArrayList();

		// Token: 0x04000038 RID: 56
		private OwnBoneInfo rootBone;

		// Token: 0x04000039 RID: 57
		private List<Transform> ragdollBonesTransformsOriginalList = new List<Transform>();

		// Token: 0x0400003A RID: 58
		public static OwnRagdollBuilder Singleton;

		// Token: 0x0400003B RID: 59
		private int previousSelectedMainButton = -1;

		// Token: 0x0400003C RID: 60
		private Vector2 actualScrollViewForBonesPosition = Vector2.zero;

		// Token: 0x0400003D RID: 61
		private Vector2 actualScrollViewForProfilesPosition = Vector2.zero;

		// Token: 0x0400003E RID: 62
		private Vector2 actualScrollViewForHelpPosition = Vector2.zero;

		// Token: 0x0400003F RID: 63
		private bool weHaveTheFocus = true;

		// Token: 0x04000040 RID: 64
		private GameObject lastSelectedActiveGameObject;

		// Token: 0x04000041 RID: 65
		private OwnRagdollBuilder window;

		// Token: 0x04000042 RID: 66
		private bool firstTime = true;

		// Token: 0x02000005 RID: 5
		// (Invoke) Token: 0x06000051 RID: 81
		private delegate void DelegateButtonFunction();

		// Token: 0x02000006 RID: 6
		// (Invoke) Token: 0x06000055 RID: 85
		private delegate void DelegateUpdateFunction();

		// Token: 0x02000007 RID: 7
		// (Invoke) Token: 0x06000059 RID: 89
		private delegate void DelegateOnDestroyFunction();
	}
}
